# Mixly 岭师人工智能素养教育共同体-自定义库

#### 介绍

Mixly平台【 **岭师人工智能素养教育共同体** 】自定义库。本仓库会随时更新基于Mixly平台开发的图形化积木，请大家持续关注本仓库的内容，更新消息会在本团队的技术交流群和微信公众号进行提醒。

### 软件架构

基于 Mixly2.0 RC3 开发。

本开源系列的测试代码在Mixly2.0 RC3版本和ESP32E上测试通过，测试所用主控板为（此主控板同时支持Mind+和Mixly）：

https://item.taobao.com/item.htm?ft=t&id=677654692365

其他ESP32E主控板暂未测试。另请注意ESP32其他系列主控与ESP32E并不兼容，不保证其他主控适配本测试代码。

### 安装教程

利用【  **https://gitee.com/LNSFAIoT/Mixly-LNSFAIoT-custom-library.git**  】克隆到本地文件夹，打开 mixly.exe 官方软件导入库文件，库文件的后缀名为【 .xml 】，利用本开源项目配套提供的测试代码进行测试。

### 使用说明

#### 积木截图

 **Mixly【岭师人工智能素养教育共同体】自定义库目录** 

![输入图片说明](image/Mixly%E3%80%90%E5%B2%AD%E5%B8%88%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E7%B4%A0%E5%85%BB%E6%95%99%E8%82%B2%E5%85%B1%E5%90%8C%E4%BD%93%E3%80%91%E8%87%AA%E5%AE%9A%E4%B9%89%E5%BA%93%E6%88%AA%E5%9B%BE.png)

 **LED点阵大屏显示** 

测试硬件

由于市面上点阵屏的厂家和芯片方案多如牛毛，专业的LED控制卡都很难兼容各种屏幕。同时市面上生产的LED方案随时会改变，无法保证自购硬件的兼容性。

本团队自主研发的点阵库同样  **只支持固定的一些点阵屏型号**  和  **某些ESP32主控**  。

因此，为了避免误导和试错，本库  **只对我们测试通过的主控和屏幕型号开放**  。大家可以通过下方的淘宝链接，购买我们测试通过的LED屏，再联系客服提供测试代码和库文件。

感谢理解！

https://item.taobao.com/item.htm?ft=t&id=677654692365

 **LED大屏显示 可级联 提供图形化编程 自研 中小学 人工智能 创客** 

![输入图片说明](image/LED%E7%82%B9%E9%98%B5%E5%A4%A7%E5%B1%8F%E6%98%BE%E7%A4%BA.png)

 **RS485多合一空气质量传感器** 

测试硬件 ：

https://shop221040643.taobao.com

多合一空气质量传感器 图形化编程 自研 提供源码 中小学 创客 AI

https://item.taobao.com/item.htm?id=678612499027

![输入图片说明](image/RS485%E5%A4%9A%E5%90%88%E4%B8%80%E7%A9%BA%E6%B0%94%E8%B4%A8%E9%87%8F%E4%BC%A0%E6%84%9F%E5%99%A8.png)

 **UART多合一空气质量传感器** 

测试硬件 ：

https://shop221040643.taobao.com

多合一空气质量传感器 图形化编程 自研 提供源码 中小学 创客 AI

https://item.taobao.com/item.htm?id=678612499027

![输入图片说明](image/UART%E5%A4%9A%E5%90%88%E4%B8%80%E7%A9%BA%E6%B0%94%E8%B4%A8%E9%87%8F%E4%BC%A0%E6%84%9F%E5%99%A8.png)

 **RS485多合一气象传感器** 

测试硬件 ：

https://shop221040643.taobao.com

多合一气象传感器 图形化编程 提供源码 中小学 人工智能 创客

https://item.taobao.com/item.htm?ft=t&id=678279094067

![输入图片说明](image/RS485%E5%A4%9A%E5%90%88%E4%B8%80%E6%B0%94%E8%B1%A1%E4%BC%A0%E6%84%9F%E5%99%A8.png)

 **RS485多合一土壤传感器** 

测试硬件 ：

https://shop221040643.taobao.com

多合一土质检测传感器 图形化编程 提供源码 中小学 人工智能 创客

https://item.taobao.com/item.htm?ft=t&id=678279094067

![输入图片说明](image/RS485%E5%A4%9A%E5%90%88%E4%B8%80%E5%9C%9F%E5%A3%A4%E4%BC%A0%E6%84%9F%E5%99%A8.png)

 **红外发射学习模块** 

![输入图片说明](image/%E7%BA%A2%E5%A4%96%E5%8F%91%E5%B0%84%E5%AD%A6%E4%B9%A0%E6%A8%A1%E5%9D%97.png)

 **离线语音模块** 

![输入图片说明](image/%E7%A6%BB%E7%BA%BF%E8%AF%AD%E9%9F%B3%E6%A8%A1%E5%9D%97.png)

 **(语音小助手)红外发射学习模块** 

![输入图片说明](image/(%E8%AF%AD%E9%9F%B3%E5%B0%8F%E5%8A%A9%E6%89%8B)%E7%BA%A2%E5%A4%96%E5%8F%91%E5%B0%84%E5%AD%A6%E4%B9%A0%E6%A8%A1%E5%9D%97.png)

 **(语音小助手)离线语音模块** 

![输入图片说明](image/(%E8%AF%AD%E9%9F%B3%E5%B0%8F%E5%8A%A9%E6%89%8B)%E7%A6%BB%E7%BA%BF%E8%AF%AD%E9%9F%B3%E6%A8%A1%E5%9D%97.png)

 **物联网平台-WiFi** 

![输入图片说明](image/WiFi%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0.png)

 **物联网平台-阿里云** 

![输入图片说明](image/%E9%98%BF%E9%87%8C%E4%BA%91%E7%89%A9%E8%81%94%E7%BD%91%E5%B9%B3%E5%8F%B0.png)

 **单线串口MP3模块** 

![输入图片说明](image/%E5%8D%95%E7%BA%BF%E4%B8%B2%E5%8F%A3MP3%E6%A8%A1%E5%9D%97.png)

### 参与贡献

 **岭师人工智能素养教育共同体** 

### 各平台官方网址

 **微信公众号：人工智能素养教育共同体** 

 **CSDN官方网址：https://blog.csdn.net/LNSFAIoT** 

 **Gitee官方网址：https://gitee.com/LNSFAIoT** 

 **DFRobot论坛官方网址：https://mc.dfrobot.com.cn** 

### 如何加入

 **请关注微信公众号：人工智能素养教育共同体。** 

![输入图片说明](image/%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E7%B4%A0%E5%85%BB%E6%95%99%E8%82%B2%E5%85%B1%E5%90%8C%E4%BD%93%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)

### 捐助

如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。

![输入图片说明](image/%E8%AF%B7%E5%A4%9A%E5%A4%9A%E6%94%AF%E6%8C%81%E6%88%91%E4%BB%AC.jpg)

### 联系我们

有关 **意见反馈** 和 **合作意向** 联系，可扫码添加以下 **企业微信** 。

#### 黄老师：

![输入图片说明](image/%E9%BB%84%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)

#### 曾老师：

![输入图片说明](image/%E6%9B%BE%E8%80%81%E5%B8%88-%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E5%90%8D%E7%89%87.jpg)