/***************************************************
  Copyright (c) : HuangHuijuan from LNSF_AIoT on 2022/07/20.
    MOD : HuangHuijuan from LNSF_AIoT on 2023/03/04.

 ****************************************************/

#include <LNSFAIoT_weather_sensor.h>

float LNSFAIoT_num_windspeed, LNSFAIoT_num_winddirection_angle, LNSFAIoT_num_ShiDu, LNSFAIoT_num_WenDu, LNSFAIoT_num_noise, LNSFAIoT_num_airpressure, LNSFAIoT_num_light;
int LNSFAIoT_num_windpower, LNSFAIoT_num_winddirection_gear;
unsigned char wenurxunweather[8] = {0x01, 0x03, 0x01, 0xf4, 0x00, 0x0e, 0x84, 0x00};
int weatheraddr = 1;
int SerialReciveData_weather[31];
void CalcCrc(unsigned char CrcBuf, int *pCrc)
{
    int g_uCode;
    *pCrc = *pCrc ^ CrcBuf;
    for (int g_Counter1 = 0; g_Counter1 < 8; g_Counter1++)
    {
        g_uCode = *pCrc & 1;
        *pCrc = *pCrc >> 1;
        *pCrc = *pCrc & 0x7fff;
        if (g_uCode == 1)
            *pCrc = *pCrc ^ 0xa001;
        *pCrc = *pCrc & 0xffff;
    }
}

int CRC16(unsigned char *pText, int DataLen)
{
    int m_wRtn = 0xffff;

    for (int g_Counter = 0; g_Counter < (int)DataLen; g_Counter++)
        CalcCrc(pText[g_Counter], &m_wRtn);

    return m_wRtn;
}

void LNSFAIoT_weather_sensor::begin(Stream *serial, int addr)
{
    delay(1000);
    mywindSerial = serial;
    setask(addr);
    weatheraddr = addr;
}

void setask(int32_t addr)
{

    wenurxunweather[0] = addr & 0xFF;
    int crc16 = CRC16(wenurxunweather, 6);
    wenurxunweather[6] = crc16 & 0x00FF;
    wenurxunweather[7] = crc16 >> 8;
}

void LNSFAIoT_weather_sensor::LNSFAIoT_weather_sensor_analysis(void)
{
    delay(800);
    mywindSerial->flush();
    for (int i = 0; i < 8; i++)
    {
        mywindSerial->write(wenurxunweather[i]);
    }
    delay(200);
    if (mywindSerial->available())
    {
        memset(SerialReciveData_weather, 0, sizeof(SerialReciveData_weather));
        int len = 0;
        while (mywindSerial->available())
        {
            SerialReciveData_weather[len] = mywindSerial->read();
            len++;
        }
    }
    if (SerialReciveData_weather[0] == weatheraddr)
    {
        LNSFAIoT_num_windspeed = ((String(SerialReciveData_weather[3]).toFloat()) * 256 + String(SerialReciveData_weather[4]).toFloat()) / 100.0;
        LNSFAIoT_num_windpower = (String(SerialReciveData_weather[5]).toInt()) * 256 + String(SerialReciveData_weather[6]).toInt();
        LNSFAIoT_num_winddirection_gear = (String(SerialReciveData_weather[7]).toInt()) * 256 + String(SerialReciveData_weather[8]).toInt();
        LNSFAIoT_num_winddirection_angle = (String(SerialReciveData_weather[9]).toInt()) * 256 + String(SerialReciveData_weather[10]).toInt();
        LNSFAIoT_num_ShiDu = ((String(SerialReciveData_weather[11]).toFloat()) * 256 + (String(SerialReciveData_weather[12]).toFloat())) / 10.0;
        LNSFAIoT_num_WenDu = ((String(SerialReciveData_weather[13]).toFloat()) * 256 + (String(SerialReciveData_weather[14]).toFloat())) / 10.0;
        LNSFAIoT_num_noise = ((String(SerialReciveData_weather[15]).toFloat()) * 256 + (String(SerialReciveData_weather[16]).toFloat())) / 10.0;
        LNSFAIoT_num_airpressure = ((String(SerialReciveData_weather[21]).toFloat()) * 256 + (String(SerialReciveData_weather[22]).toFloat())) / 10.0;
        LNSFAIoT_num_light = ((String(SerialReciveData_weather[27]).toFloat()) * 256 + (String(SerialReciveData_weather[28]).toFloat()));
        // LNSFAIoT_num_rain = ((String(SerialReciveData_weather[29]).toFloat()) * 256 + (String(SerialReciveData_weather[30]).toFloat()));
        memset(SerialReciveData_weather, 0, sizeof(SerialReciveData_weather));
    }
}

void LNSFAIoT_weather_sensor::LNSFAIoT_weather_sensor_println(void)
{
    Serial.println("多合一气象参数:");
    Serial.println("风速：" + String(LNSFAIoT_num_windspeed) + " m/s");
    Serial.println("风力：" + String(LNSFAIoT_num_windpower));
    Serial.println("风向(档位)：" + String(LNSFAIoT_num_winddirection_gear));
    Serial.println("风向：" + String(LNSFAIoT_num_winddirection_angle) + " °");
    Serial.println("湿度：" + String(LNSFAIoT_num_ShiDu) + " %RH");
    Serial.println("温度：" + String(LNSFAIoT_num_WenDu) + " ℃");
    Serial.println("噪声：" + String(LNSFAIoT_num_noise) + " dB");
    Serial.println("大气压力：" + String(LNSFAIoT_num_airpressure) + " Kpa");
    Serial.println("光照：" + String(LNSFAIoT_num_light) + " 百Lux");
}

float LNSFAIoT_weather_sensor::LNSFAIoT_num_windspeed_func(void)
{
    return LNSFAIoT_num_windspeed;
}

int LNSFAIoT_weather_sensor::LNSFAIoT_num_windpower_func(void)
{
    return LNSFAIoT_num_windpower;
}

int LNSFAIoT_weather_sensor::LNSFAIoT_num_winddirection_gear_func(void)
{
    return LNSFAIoT_num_winddirection_gear;
}

float LNSFAIoT_weather_sensor::LNSFAIoT_num_winddirection_angle_func(void)
{
    return LNSFAIoT_num_winddirection_angle;
}

float LNSFAIoT_weather_sensor::LNSFAIoT_num_ShiDu_func(void)
{
    return LNSFAIoT_num_ShiDu;
}

float LNSFAIoT_weather_sensor::LNSFAIoT_num_WenDu_func(void)
{
    return LNSFAIoT_num_WenDu;
}

float LNSFAIoT_weather_sensor::LNSFAIoT_num_noise_func(void)
{
    return LNSFAIoT_num_noise;
}

float LNSFAIoT_weather_sensor::LNSFAIoT_num_light_func(void)
{
    return LNSFAIoT_num_light;
}

float LNSFAIoT_weather_sensor::LNSFAIoT_num_airpressure_func(void)
{
    return LNSFAIoT_num_airpressure;
}