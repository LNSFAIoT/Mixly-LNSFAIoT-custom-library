/***************************************************
  Written by HuangHuijuan from LNSF_AIoT on 2022/07/14.
    MOD : HuangHuijuan from LNSF_AIoT on 2023/03/04.

 ****************************************************/
#ifndef _LNSFAIOT_WEATHER_QUALITY_SERSOR
#define _LNSFAIOT_WEATHER_QUALITY_SERSOR

#include "Arduino.h"

#if ARDUINO >= 100
#define SERIAL_WRITE(...) mywindSerial->write(__VA_ARGS__)
#else
#define SERIAL_WRITE(...) mywindSerial->write(__VA_ARGS__, BYTE)
#endif

#define SERIAL_WRITE_U16(v)        \
  SERIAL_WRITE((uint8_t)(v >> 8)); \
  SERIAL_WRITE((uint8_t)(v & 0xFF));

void CalcCrc(unsigned char CrcBuf, int *pCrc);
int CRC16(unsigned char *pText, int DataLen);
void setask(int32_t addr);

class LNSFAIoT_weather_sensor
{

public:
  void begin(Stream *serial,int addr);

  void LNSFAIoT_weather_sensor_analysis(void);
  void LNSFAIoT_weather_sensor_println(void);
  float LNSFAIoT_num_windspeed_func(void);
  int LNSFAIoT_num_windpower_func(void);
  int LNSFAIoT_num_winddirection_gear_func(void);
  float LNSFAIoT_num_winddirection_angle_func(void);
  float LNSFAIoT_num_ShiDu_func(void);
  float LNSFAIoT_num_WenDu_func(void);
  float LNSFAIoT_num_noise_func(void);
  float LNSFAIoT_num_light_func(void);
  float LNSFAIoT_num_airpressure_func(void);

private:
  Stream *mywindSerial;
};
#endif
