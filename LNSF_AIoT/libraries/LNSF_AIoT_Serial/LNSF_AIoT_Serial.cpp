
#define G29 void LNSF_AIoT_Serial::SendFrameHead(String framehead, uint8_t newnum)
#define G28 void LNSF_AIoT_Serial::SendCharAndUnsigned(char chardata)
#define G27 void LNSF_AIoT_Serial::SendFrameTail(String frametail)
#define G26 void LNSF_AIoT_Serial::SendDouble(double doubledata)
#define G25 char *foot = const_cast<char *>(frametail.c_str());
#define G24 char *head = const_cast<char *>(framehead.c_str());
#define G23 void LNSF_AIoT_Serial::begin(Stream *serial)
#define G22 void LNSF_AIoT_Serial::ReadHEX2String(void)
#define G21 for (int i = 0; i < strlen(head) / 2; i++)
#define G20 for (int j = 0; j < strlen(foot) / 2; j++)
#define G1F String LNSF_AIoT_Serial::Serialdata(void)
#define G1E void LNSF_AIoT_Serial::SendInt(int num1)
#define G1D AsciiToHex(head, shuju, strlen(head));
#define G1C AsciiToHex(foot, SHU, strlen(foot));
#define G1B byte array[1] = {mySerial->read()};
#define G1A doubletobytes(doubledata, ybt1);
#define G19 array_to_string(array, 1, str);
#define G18 IntToBytes(num1, bytes1, 10);
#define G17 while (mySerial->available())
#define G16 for (int i = 0; i < 4; i++)
#define G15 for (int i = 0; i < 8; i++)
#define G14 mySerial->write(bytes1[i]);
#define G13 mySerial->write(chardata);
#define G12 if (mySerial->available())
#define G11 mySerial->write(shuju[i]);
#define G10 mySerial->write(ybt1[i]);
#define GF unsigned char bytes1[10];
#define GE mySerial->write(newnum);
#define GD comdata = comdata + str;
#define GC unsigned char shuju[50];
#define GB mySerial->write(SHU[j]);
#define GA unsigned char SHU[50];
#define G9 String comdata = "";
#define G8 mySerial = serial;
#define G7 if (comdata != "")
#define G6 char str[4] = "";
#define G5 return comdata;
#define G4 comdata = "";
#define G3 byte ybt1[8];
#define G2 delay(1000);
#define G1 {
#define G0 }
#include "LNSF_AIoT_Serial.h"
#define G2A G23 G1 G2 G8 G0 G9 G22 G1 G7 G1
#define G2B G4 G0 G12 G1 G17 G1 G1B G6 G19 GD
#define G2C G0 G0 G0 G1F G1 G5 G0 G29 G1 G24
#define G2D GC G1D G21 G1 G11 G0 GE G0 G27 G1
#define G2E G25 GA G1C G20 G1 GB G0 G0 G26 G1
#define G2F G3 G1A G15 G1 G10 G0 G0 G1E G1 GF
#define G30 G18 G16 G1 G14 G0 G0 G28 G1 G13 G0
#define G31 G2A G2B G2C G2D G2E G2F G30 
#define G32(__FOX__) __FOX__
G32(G31)