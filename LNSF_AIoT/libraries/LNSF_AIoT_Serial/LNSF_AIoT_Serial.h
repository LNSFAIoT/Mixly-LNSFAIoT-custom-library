/***************************************************
  Written by FoXzEnltH from LNSF_AIoT on 2022/07/12.
  Alter by HuangHuijuan from LNSF_AIoT on 2022/07/14.
 ****************************************************/
#ifndef _LNSF_AIOT_SERIAL
#define _LNSF_AIOT_SERIAL

#include "Arduino.h"
#include "LNSF_AIoT_TypeChange.h"
#if defined(__AVR__) || defined(ESP8266) || defined(FREEDOM_E300_HIFIVE1)
#include <SoftwareSerial.h>
#endif


#if ARDUINO >= 100
#define SERIAL_WRITE(...) mySerial->write(__VA_ARGS__)
#else
#define SERIAL_WRITE(...) mySerial->write(__VA_ARGS__, BYTE)
#endif

#define SERIAL_WRITE_U16(v)        \
  SERIAL_WRITE((uint8_t)(v >> 8)); \
  SERIAL_WRITE((uint8_t)(v & 0xFF));

class LNSF_AIoT_Serial
{
public:
  // void begin(HardwareSerial *serial, int rx, int tx, int bo);
  // #if defined(__AVR__) || defined(ESP8266) || defined(FREEDOM_E300_HIFIVE1)
  // void begin(SoftwareSerial *serial, int bo);
// #endif
void begin(Stream *serial);
  void SendFrameHead(String framehead, uint8_t newnum);
  void SendFrameTail(String frametail);
  void SendDouble(double doubledata);
  void SendInt(int num1);
  void SendCharAndUnsigned(char chardata);
  void ReadHEX2String(void);
  String Serialdata(void);
private:

// #if defined(__AVR__) || defined(ESP8266) || defined(FREEDOM_E300_HIFIVE1)
  // SoftwareSerial *serialS;
// #endif
  // HardwareSerial *serialH;
  Stream *mySerial;
};
#endif
