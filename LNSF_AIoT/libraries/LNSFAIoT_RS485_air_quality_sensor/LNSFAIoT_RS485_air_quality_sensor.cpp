/***************************************************
  Copyright (c) : HuangHuijuan from LNSF_AIoT on 2022/07/20.
  MOD : HuangHuijuan from LNSF_AIoT on 2023/03/04.
 ****************************************************/

#include <LNSFAIoT_RS485_air_quality_sensor.h>

unsigned int LNSFAIoT_air_num_PM10, LNSFAIoT_air_num_CO2, LNSFAIoT_air_num_CH2O, LNSFAIoT_air_num_TVOC, LNSFAIoT_air_num_PM25;
float LNSFAIoT_air_num_WenDu, LNSFAIoT_air_num_ShiDu;
int32_t wenurxunair[8] = {0x01, 0x03, 0x00, 0x02, 0x00, 0x07, 0xA5, 0xC8};
int SerialReciveData_air[30];

void LNSFAIoT_RS485_air_quality_sensor::beginser(Stream &stream, int addrin)
{
    delay(1000);
    myairSerial = &stream;
    switch (addrin)
    {
    case 1:
        wenurxunair[0] = 0x01;
        wenurxunair[6] = 0xA5;
        wenurxunair[7] = 0xC8;
        break;
    case 2:
        wenurxunair[0] = 0x02;
        wenurxunair[6] = 0xA5;
        wenurxunair[7] = 0xFB;
        break;
    case 3:
        wenurxunair[0] = 0x03;
        wenurxunair[6] = 0xA4;
        wenurxunair[7] = 0x2A;
        break;
    case 4:
        wenurxunair[0] = 0x04;
        wenurxunair[6] = 0xA5;
        wenurxunair[7] = 0x9D;
        break;
    case 5:
        wenurxunair[0] = 0x05;
        wenurxunair[6] = 0xA4;
        wenurxunair[7] = 0x4C;
        break;
    case 6:
        wenurxunair[0] = 0x06;
        wenurxunair[6] = 0xA4;
        wenurxunair[7] = 0x7F;
        break;
    case 7:
        wenurxunair[0] = 0x07;
        wenurxunair[6] = 0xA5;
        wenurxunair[7] = 0xAE;
        break;
    default:
        break;
    }
}

void LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_quality_sensor_RS485analysis(void)
{
    delay(800);
    myairSerial->flush();
    for (int i = 0; i < 6; i++)
    {
        myairSerial->write(wenurxunair[i]);
    }
    delay(200);
    if (myairSerial->available())
    {
        memset(SerialReciveData_air, 0, sizeof(SerialReciveData_air));
        int len = 0;
        while (myairSerial->available())
        {
            SerialReciveData_air[len] = myairSerial->read();
            len++;
        }
    }
    if (SerialReciveData_air[0] == wenurxunair[0])
    {
        LNSFAIoT_air_num_CO2 = (String(SerialReciveData_air[3]).toFloat()) * 256 + String(SerialReciveData_air[4]).toFloat();
        LNSFAIoT_air_num_CH2O = (String(SerialReciveData_air[5]).toInt()) * 256 + String(SerialReciveData_air[6]).toInt();
        LNSFAIoT_air_num_TVOC = (String(SerialReciveData_air[7]).toInt()) * 256 + String(SerialReciveData_air[8]).toInt();
        LNSFAIoT_air_num_PM25 = (String(SerialReciveData_air[9]).toFloat()) * 256 + String(SerialReciveData_air[10]).toFloat();
        LNSFAIoT_air_num_PM10 = (String(SerialReciveData_air[11]).toFloat()) * 256 + String(SerialReciveData_air[12]).toFloat();
        LNSFAIoT_air_num_WenDu = ((String(SerialReciveData_air[13]).toFloat()) * 256 + (String(SerialReciveData_air[14]).toFloat())) * 0.1;
        LNSFAIoT_air_num_ShiDu = ((String(SerialReciveData_air[15]).toFloat()) * 256 + (String(SerialReciveData_air[16]).toFloat())) * 0.1;
        memset(SerialReciveData_air, 0, sizeof(SerialReciveData_air));
    }
}

void LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_quality_sensor_RS485println(void)
{
    Serial.println("多合一空气质量参数:");
    Serial.println("温度: " + String(LNSFAIoT_air_num_WenDu) + " ℃");
    Serial.println("湿度: " + String(LNSFAIoT_air_num_ShiDu) + " %");
    Serial.println("PM10: " + String(LNSFAIoT_air_num_PM10) + " ug/m3");
    Serial.println("CO2: " + String(LNSFAIoT_air_num_CO2) + " ppm");
    Serial.println("CH2O: " + String(LNSFAIoT_air_num_CH2O) + " ug/m3");
    Serial.println("TVOC: " + String(LNSFAIoT_air_num_TVOC) + " ug/m3");
    Serial.println("PM2.5: " + String(LNSFAIoT_air_num_PM25) + " ug/m3");
}

float LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_num_ShiDu_RS485func(void)
{
    return LNSFAIoT_air_num_ShiDu;
}

float LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_num_WenDu_RS485func(void)
{
    return LNSFAIoT_air_num_WenDu;
}

unsigned int LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_num_PM10_RS485func(void)
{
    return LNSFAIoT_air_num_PM10;
}

unsigned int LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_num_CO2_RS485func(void)
{
    return LNSFAIoT_air_num_CO2;
}

unsigned int LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_num_CH2O_RS485func(void)
{
    return LNSFAIoT_air_num_CH2O;
}

unsigned int LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_num_TVOC_RS485func(void)
{
    return LNSFAIoT_air_num_TVOC;
}

unsigned int LNSFAIoT_RS485_air_quality_sensor::LNSFAIoT_air_num_PM25_RS485func(void)
{
    return LNSFAIoT_air_num_PM25;
}