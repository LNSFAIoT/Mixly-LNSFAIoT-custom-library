/***************************************************
  Written by HuangHuijuan from LNSF_AIoT on 2022/07/14.
    MOD : HuangHuijuan from LNSF_AIoT on 2023/03/04.
 ****************************************************/
#ifndef _LNSFAIOT_RS485_AIR_QUALITY_SERSOR
#define _LNSFAIOT_RS485_AIR_QUALITY_SERSOR

#include "Arduino.h"

#if ARDUINO >= 100
#define SERIAL_WRITE(...) myairSerial->write(__VA_ARGS__)
#else
#define SERIAL_WRITE(...) myairSerial->write(__VA_ARGS__, BYTE)
#endif

#define SERIAL_WRITE_U16(v)        \
  SERIAL_WRITE((uint8_t)(v >> 8)); \
  SERIAL_WRITE((uint8_t)(v & 0xFF));

class LNSFAIoT_RS485_air_quality_sensor
{
public:
  void LNSFAIoT_air_quality_sensor_RS485analysis(void);
  void beginser(Stream &stream, int addr);
  void LNSFAIoT_air_quality_sensor_RS485println(void);
  float LNSFAIoT_air_num_ShiDu_RS485func(void);
  float LNSFAIoT_air_num_WenDu_RS485func(void);
  unsigned int LNSFAIoT_air_num_PM10_RS485func(void);
  unsigned int LNSFAIoT_air_num_CO2_RS485func(void);
  unsigned int LNSFAIoT_air_num_CH2O_RS485func(void);
  unsigned int LNSFAIoT_air_num_TVOC_RS485func(void);
  unsigned int LNSFAIoT_air_num_PM25_RS485func(void);

private:
  Stream *myairSerial;
};
#endif
