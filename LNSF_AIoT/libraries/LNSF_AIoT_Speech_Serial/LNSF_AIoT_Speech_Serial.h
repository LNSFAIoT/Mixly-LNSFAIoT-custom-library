/*!
 *@file LNSF_AIoT_Speech_Serial.cpp
 *@brief Processing of serial data for AI intelligent offline speech recognition and speech synthesis.
 *@licence     The MIT License (MIT)
 *@Contact 微信公众号:人工智能素养教育共同体
 *@author foxzenith benxiaohai 
 *@version  V1.0
 *@date  2022-7-25
 *@https://gitee.com/LNSFAIoT/offline-speech-recognition-and-synthesis-modules
*/ 

#ifndef _LNSF_AIOT_SPEECH_SERIAL
#define _LNSF_AIOT_SPEECH_SERIAL

#include "Arduino.h"
#include "LNSF_AIoT_Type.h"

#if defined(__AVR__) || defined(ESP8266) || defined(FREEDOM_E300_HIFIVE1)
#include <SoftwareSerial.h>
#endif


#if ARDUINO >= 100
#define SERIAL_WRITE(...) mySerial->write(__VA_ARGS__)
#else
#define SERIAL_WRITE(...) mySerial->write(__VA_ARGS__, BYTE)
#endif

#define SERIAL_WRITE_U16(v)        \
  SERIAL_WRITE((uint8_t)(v >> 8)); \
  SERIAL_WRITE((uint8_t)(v & 0xFF));

class LNSF_AIoT_Speech_Serial
{
public:
//   void begin(HardwareSerial *serial, int rx, int tx, int bo);
//   #if defined(__AVR__) || defined(ESP8266) || defined(FREEDOM_E300_HIFIVE1)
//   void begin(SoftwareSerial *serial, int bo);
// #endif
  void begin(Stream *serial);
  void SendFrameHead(String framehead, uint8_t newnum);
  void SendFrameTail(String frametail);
  void SendDouble(float floatdata);
  void SendInt(int num1);
  void SendCharAndUnsigned(char chardata);
  void ReadHEX2String(void);
  void Sendhead(void);
  void Sendfoot(void);
  void SendNumber(uint8_t num);
  String Serialdata(void);
private:
// #if defined(__AVR__) || defined(ESP8266) || defined(FREEDOM_E300_HIFIVE1)
//   SoftwareSerial *serialS;
// #endif
//   HardwareSerial *serialH;
  Stream *mySerial;
};
#endif
