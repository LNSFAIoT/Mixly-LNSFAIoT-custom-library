/***************************************************
  Written by HuangHuijuan from LNSF_AIoT on 2022/07/14.
    MOD : HuangHuijuan from LNSF_AIoT on 2023/03/04.
 ****************************************************/
#ifndef _LNSFAIOT_SOIL_SENSOR
#define _LNSFAIOT_SOIL_SENSOR

#include "Arduino.h"

#if ARDUINO >= 100
#define SERIAL_WRITE(...) mysoilSerial->write(__VA_ARGS__)
#else
#define SERIAL_WRITE(...) mysoilSerial->write(__VA_ARGS__, BYTE)
#endif

#define SERIAL_WRITE_U16(v)        \
  SERIAL_WRITE((uint8_t)(v >> 8)); \
  SERIAL_WRITE((uint8_t)(v & 0xFF));

void soilCalcCrc(unsigned char CrcBuf, int *pCrc);
int soilCRC16(unsigned char *pText, int DataLen);
void soilsetask(int32_t addr);

class LNSFAIoT_soil_sensor
{

public:
  void begin(Stream *serial,int addr);

  void LNSFAIoT_soil_sensor_println(void);
  void LNSFAIoT_soil_sensor_analysis(void);
  float LNSFAIoT_soil_num_PH_func(void);
  int LNSFAIoT_soil_num_DianDaoLv_func(void);
  float LNSFAIoT_soil_num_WenDu_func(void);
  float LNSFAIoT_soil_num_ShiDu_func(void);

private:
  Stream *mysoilSerial;
};
#endif
