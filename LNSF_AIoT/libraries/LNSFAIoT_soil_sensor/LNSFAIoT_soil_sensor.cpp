/***************************************************
  Copyright (c) : HuangHuijuan from LNSF_AIoT on 2022/07/14.
    MOD : HuangHuijuan from LNSF_AIoT on 2023/03/04.
 ****************************************************/

#include <LNSFAIoT_soil_sensor.h>

float LNSFAIoT_soil_num_ShiDu, LNSFAIoT_soil_num_WenDu, LNSFAIoT_soil_num_PH;
int LNSFAIoT_soil_num_N, LNSFAIoT_soil_num_P, LNSFAIoT_soil_num_K, LNSFAIoT_soil_num_DianDaoLv;
unsigned char wenxunsoil[8] = {0x01, 0x03, 0x00, 0x00, 0x00, 0x07, 0x04, 0x08};
int soiladdr = 1;
int SerialReciveData_soil[30];
void soilCalcCrc(unsigned char CrcrBuf, int *pCrc)
{
    int g_uCode;
    *pCrc = *pCrc ^ CrcrBuf;
    for (int g_Counter1 = 0; g_Counter1 < 8; g_Counter1++)
    {
        g_uCode = *pCrc & 1;
        *pCrc = *pCrc >> 1;
        *pCrc = *pCrc & 0x7fff;
        if (g_uCode == 1)
            *pCrc = *pCrc ^ 0xa001;
        *pCrc = *pCrc & 0xffff;
    }
}

int soilCRC16(unsigned char *pText, int DataLen)
{
    int m_wRtn = 0xffff;
    for (int g_Counter = 0; g_Counter < (int)DataLen; g_Counter++)
        soilCalcCrc(pText[g_Counter], &m_wRtn);
    return m_wRtn;
}

void LNSFAIoT_soil_sensor::begin(Stream *serial, int addr)
{
    delay(1000);
    mysoilSerial = serial;
    soilsetask(addr);
    soiladdr = addr;
}

void soilsetask(int32_t addr)
{
    wenxunsoil[0] = addr & 0xFF;
    int crc16 = soilCRC16(wenxunsoil, 6);
    wenxunsoil[6] = crc16 & 0x00FF;
    wenxunsoil[7] = crc16 >> 8;
}

void LNSFAIoT_soil_sensor::LNSFAIoT_soil_sensor_analysis(void)
{
    delay(800);
    mysoilSerial->flush();
    for (int i = 0; i < 8; i++)
    {
        mysoilSerial->write(wenxunsoil[i]);
    }
    delay(200);
    if (mysoilSerial->available())
    {
        memset(SerialReciveData_soil, 0, sizeof(SerialReciveData_soil));
        int len = 0;
        while (mysoilSerial->available())
        {
            SerialReciveData_soil[len] = mysoilSerial->read();
            len++;
        }
    }
    if (SerialReciveData_soil[0] == soiladdr)
    {
        LNSFAIoT_soil_num_ShiDu = ((String(SerialReciveData_soil[3]).toFloat() * 256 + String(SerialReciveData_soil[4]).toFloat()) / 10);
        LNSFAIoT_soil_num_WenDu = ((String(SerialReciveData_soil[5]).toFloat() * 256 + String(SerialReciveData_soil[6]).toFloat()) / 10);
        LNSFAIoT_soil_num_DianDaoLv = (String(SerialReciveData_soil[7]).toFloat() * 256 + String(SerialReciveData_soil[8]).toFloat());
        LNSFAIoT_soil_num_PH = ((String(SerialReciveData_soil[9]).toFloat() * 256 + String(SerialReciveData_soil[10]).toFloat()) / 10);
        LNSFAIoT_soil_num_N = (String(SerialReciveData_soil[11]).toFloat() * 256 + String(SerialReciveData_soil[12]).toFloat());
        LNSFAIoT_soil_num_P = (String(SerialReciveData_soil[13]).toFloat() * 256 + String(SerialReciveData_soil[14]).toFloat());
        LNSFAIoT_soil_num_K = (String(SerialReciveData_soil[15]).toFloat() * 256 + String(SerialReciveData_soil[16]).toFloat());
        memset(SerialReciveData_soil, 0, sizeof(SerialReciveData_soil));
    }
}

void LNSFAIoT_soil_sensor::LNSFAIoT_soil_sensor_println(void)
{
    Serial.println("多合一土壤参数:");
    Serial.println("土壤温度：" + String(LNSFAIoT_soil_num_WenDu) + " ℃");
    Serial.println("土壤湿度/水分/含水率：" + String(LNSFAIoT_soil_num_ShiDu) + " %");
    Serial.println("土壤电导率：" + String(LNSFAIoT_soil_num_DianDaoLv) + " us/cm");
    Serial.println("土壤PH：" + String(LNSFAIoT_soil_num_PH));
    // Serial.println("土壤氮：" + String(LNSFAIoT_soil_num_N) + " mg/kg或mg/L");
    // Serial.println("土壤磷：" + String(LNSFAIoT_soil_num_P) + " mg/kg或mg/L");
    // Serial.println("土壤钾：" + String(LNSFAIoT_soil_num_K) + " mg/kg或mg/L");
}

float LNSFAIoT_soil_sensor::LNSFAIoT_soil_num_ShiDu_func(void)
{
    return LNSFAIoT_soil_num_ShiDu;
}

float LNSFAIoT_soil_sensor::LNSFAIoT_soil_num_WenDu_func(void)
{
    return LNSFAIoT_soil_num_WenDu;
}

int LNSFAIoT_soil_sensor::LNSFAIoT_soil_num_DianDaoLv_func(void)
{
    return LNSFAIoT_soil_num_DianDaoLv;
}

float LNSFAIoT_soil_sensor::LNSFAIoT_soil_num_PH_func(void)
{
    return LNSFAIoT_soil_num_PH;
}