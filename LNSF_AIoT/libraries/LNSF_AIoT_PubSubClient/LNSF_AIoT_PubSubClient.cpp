
#define J122 boolean PubSubClient::connect(const char *id, const char *user, const char *pass, const char* willTopic, uint8_t willQos, boolean willRetain, const char* willMessage) {
#define J121 boolean PubSubClient::connect(const char *id, const char* willTopic, uint8_t willQos, boolean willRetain, const char* willMessage) {
#define J120 PubSubClient::PubSubClient(const char* domain, uint16_t port, MQTT_CALLBACK_SIGNATURE, Client& client, Stream& stream) {
#define J11F boolean PubSubClient::publish_P(const char* topic, const uint8_t* payload, unsigned int plength, boolean retained) {
#define J11E PubSubClient::PubSubClient(IPAddress addr, uint16_t port, MQTT_CALLBACK_SIGNATURE, Client& client, Stream& stream) {
#define J11D boolean PubSubClient::publish(const char* topic, const uint8_t* payload, unsigned int plength, boolean retained) {
#define J11C PubSubClient::PubSubClient(uint8_t *ip, uint16_t port, MQTT_CALLBACK_SIGNATURE, Client& client, Stream& stream) {
#define J11B boolean PubSubClient::publish(const char* topic, const char* payload, unsigned int plength,boolean retained) {
#define J11A PubSubClient::PubSubClient(const char* domain, uint16_t port, MQTT_CALLBACK_SIGNATURE, Client& client) {
#define J119 if ((t - lastInActivity > MQTT_KEEPALIVE*1000UL) || (t - lastOutActivity > MQTT_KEEPALIVE*1000UL)) {
#define J118 PubSubClient::PubSubClient(IPAddress addr, uint16_t port, MQTT_CALLBACK_SIGNATURE, Client& client) {
#define J117 PubSubClient::PubSubClient(uint8_t *ip, uint16_t port, MQTT_CALLBACK_SIGNATURE, Client& client) {
#define J116 boolean PubSubClient::publish(const char* topic, const uint8_t* payload, unsigned int plength) {
#define J115 PubSubClient::PubSubClient(const char* domain, uint16_t port, Client& client, Stream& stream) {
#define J114 bytesToWrite = (bytesRemaining > MQTT_MAX_TRANSFER_SIZE)?MQTT_MAX_TRANSFER_SIZE:bytesRemaining;
#define J113 PubSubClient::PubSubClient(IPAddress addr, uint16_t port, Client& client, Stream& stream) {
#define J112 boolean PubSubClient::publish(const char* topic, const char* payload, boolean retained) {
#define J111 PubSubClient::PubSubClient(uint8_t *ip, uint16_t port, Client& client, Stream& stream) {
#define J110 uint16_t PubSubClient::writeString(const char* string, uint8_t* buf, uint16_t pos) {
#define J10F boolean PubSubClient::connect(const char *id, const char *user, const char *pass) {
#define J10E PubSubClient::PubSubClient(const char* domain, uint16_t port, Client& client) {
#define J10D if(currentMillis - previousMillis >= ((int32_t) MQTT_SOCKET_TIMEOUT * 1000)){
#define J10C boolean PubSubClient::write(uint8_t header, uint8_t* buf, uint16_t length) {
#define J10B PubSubClient& PubSubClient::setServer(const char * domain, uint16_t port) {
#define J10A PubSubClient::PubSubClient(IPAddress addr, uint16_t port, Client& client) {
#define J109 PubSubClient::PubSubClient(uint8_t *ip, uint16_t port, Client& client) {
#define J108 boolean PubSubClient::publish(const char* topic, const char* payload) {
#define J107 return publish(topic,(const uint8_t*)payload,strlen(payload),retained);
#define J106 return connect(id,NULL,NULL,willTopic,willQos,willRetain,willMessage);
#define J105 return publish(topic,(const uint8_t*)payload,strlen(payload),false);
#define J104 PubSubClient& PubSubClient::setServer(IPAddress ip, uint16_t port) {
#define J103 PubSubClient& PubSubClient::setServer(uint8_t * ip, uint16_t port) {
#define J102 boolean PubSubClient::readByte(uint8_t * result, uint16_t * index){
#define J101 PubSubClient& PubSubClient::setCallback(MQTT_CALLBACK_SIGNATURE) {
#define J100 uint8_t d[9] = {0x00,0x06,'M','Q','I','s','d','p', MQTT_VERSION};
#define JFF boolean PubSubClient::subscribe(const char* topic, uint8_t qos) {
#define JFE if (t-lastInActivity >= ((int32_t) MQTT_SOCKET_TIMEOUT*1000UL)) {
#define JFD return publish(topic, (const uint8_t*)payload, plength, false);
#define JFC rc += _client->write((char)pgm_read_byte_near(payload + i));
#define JFB skip = (buffer[*lengthLength+1]<<8)+buffer[*lengthLength+2];
#define JFA if (MQTT_MAX_PACKET_SIZE < 5 + 2+strlen(topic) + plength) {
#define JF9 uint16_t PubSubClient::readPacket(uint8_t* lengthLength) {
#define JF8 uint8_t d[7] = {0x00,0x04,'M','Q','T','T',MQTT_VERSION};
#define JF7 return write(MQTTUNSUBSCRIBE|MQTTQOS1,buffer,length-5);
#define JF6 PubSubClient& PubSubClient::setStream(Stream& stream){
#define JF5 PubSubClient& PubSubClient::setClient(Client& client){
#define JF4 boolean PubSubClient::unsubscribe(const char* topic) {
#define JF3 return write(MQTTSUBSCRIBE|MQTTQOS1,buffer,length-5);
#define JF2 boolean PubSubClient::subscribe(const char* topic) {
#define JF1 result = _client->connect(this->domain, this->port);
#define JF0 uint8_t * write_address = &(result[current_index]);
#define JEF msgId = (buffer[llen+3+tl]<<8)+buffer[llen+3+tl+1];
#define JEE boolean PubSubClient::readByte(uint8_t * result) {
#define JED length = writeString((char*)topic, buffer,length);
#define JEC if (!this->stream && len > MQTT_MAX_PACKET_SIZE) {
#define JEB uint16_t tl = (buffer[llen+1]<<8)+buffer[llen+2];
#define JEA memcpy(debug_buffer,buffer,MQTT_MAX_PACKET_SIZE);
#define JE9 bool isPublish = (buffer[0]&0xF0) == MQTTPUBLISH;
#define JE8 rc = _client->write(buf+(4-llen),length+1+llen);
#define JE7 length = writeString(willMessage,buffer,length);
#define JE6 result = _client->connect(this->ip, this->port);
#define JE5 if (MQTT_MAX_PACKET_SIZE < 9 + strlen(topic)) {
#define JE4 boolean PubSubClient::connect(const char *id) {
#define JE3 return publish(topic, payload, plength, false);
#define JE2 length = writeString(willTopic,buffer,length);
#define JE1 for (j = 0;j<MQTT_HEADER_VERSION_LENGTH;j++) {
#define JE0 buffer[length++] = ((MQTT_KEEPALIVE) & 0xFF);
#define JDF PubSubClient::PubSubClient(Client& client) {
#define JDE lastInActivity = lastOutActivity = millis();
#define JDD if (isPublish && len-*lengthLength-2>skip) {
#define JDC rc = _client->write(writeBuf,bytesToWrite);
#define JDB length = writeString(topic, buffer,length);
#define JDA buffer[length++] = ((MQTT_KEEPALIVE) >> 8);
#define JD9 uint8_t* PubSubClient::getBufferPointer(){
#define JD8 length = writeString(topic,buffer,length);
#define JD7 length = writeString(pass,buffer,length);
#define JD6 length = writeString(user,buffer,length);
#define JD5 IPAddress addr(ip[0],ip[1],ip[2],ip[3]);
#define JD4 callback(topic,payload,len-llen-3-tl-2);
#define JD3 uint16_t bytesRemaining = length+1+llen;
#define JD2 memmove(buffer+llen+2,buffer+llen+3,tl);
#define JD1 for (uint16_t i = start;i<length;i++) {
#define JD0 while((bytesRemaining > 0) && result) {
#define JCF length = writeString(id,buffer,length);
#define JCE this->_state = MQTT_CONNECTION_TIMEOUT;
#define JCD callback(topic,payload,len-llen-3-tl);
#define JCC v = 0x06|(willQos<<3)|(willRetain<<5);
#define JCB buffer[length++] = (nextMsgId & 0xFF);
#define JCA return connect(id,NULL,NULL,0,0,0,0);
#define JC9 if (this->_state == MQTT_CONNECTED) {
#define JC8 return write(header,buffer,length-5);
#define JC7 length += (digit & 127) * multiplier;
#define JC6 return connect(id,user,pass,0,0,0,0);
#define JC5 if(!readByte(buffer, &len)) return 0;
#define JC4 buffer[length++] = (nextMsgId >> 8);
#define JC3 this->_state = MQTT_CONNECTION_LOST;
#define JC2 uint8_t PubSubClient::getDebugVar(){
#define JC1 char *topic = (char*) buffer+llen+2;
#define JC0 pos = writeString(topic,buffer,pos);
#define JBF if ((buffer[0]&0x06) == MQTTQOS1) {
#define JBE uint32_t previousMillis = millis();
#define JBD boolean PubSubClient::connected() {
#define JBC write(MQTTCONNECT,buffer,length-5);
#define JBB } else if (type == MQTTPINGRESP) {
#define JBA uint32_t currentMillis = millis();
#define JB9 _state = MQTT_CONNECTION_TIMEOUT;
#define JB8 uint16_t len = readPacket(&llen);
#define JB7 uint8_t* writeBuf = buf+(4-llen);
#define JB6 rc += _client->write(buffer,pos);
#define JB5 void PubSubClient::disconnect() {
#define JB4 this->_state = MQTT_DISCONNECTED;
#define JB3 if (len < MQTT_MAX_PACKET_SIZE) {
#define JB2 } else if (type == MQTTPINGREQ) {
#define JB1 void PubSubClient::dump_buffer(){
#define JB0 uint16_t current_index = *index;
#define JAF return rc == tlen + 4 + plength;
#define JAE while (!_client->available()) {
#define JAD if(!readByte(&digit)) return 0;
#define JAC rc = (int)_client->connected();
#define JAB PubSubClient::PubSubClient() {
#define JAA buffer[length++] = payload[i];
#define JA9 boolean PubSubClient::loop() {
#define JA8 while(!_client->available()) {
#define JA7 result = (rc == bytesToWrite);
#define JA6 uint8_t type = buffer[0]&0xF0;
#define JA5 _state = MQTT_CONNECT_FAILED;
#define JA4 payload = buffer+llen+3+tl+2;
#define JA3 } while ((digit & 128) != 0);
#define JA2 return (rc == 1+llen+length);
#define JA1 uint8_t header = MQTTPUBLISH;
#define JA0 if(readByte(write_address)){
#define J9F return setServer(addr,port);
#define J9E if (_client->available()) {
#define J9D _state = MQTT_DISCONNECTED;
#define J9C buffer[3] = (msgId & 0xFF);
#define J9B *index = current_index + 1;
#define J9A payload = buffer+llen+3+tl;
#define J99 return subscribe(topic, 0);
#define J98 lastOutActivity = millis();
#define J97 unsigned long t = millis();
#define J96 int PubSubClient::state() {
#define J95 this->stream->write(digit);
#define J94 buffer[0] = MQTTDISCONNECT;
#define J93 buf[5-llen+i] = lenBuf[i];
#define J92 buf[pos-i-1] = (i & 0xFF);
#define J91 if (type == MQTTPUBLISH) {
#define J90 lastInActivity = millis();
#define J8F for (int i=0;i<llen;i++) {
#define J8E this->callback = callback;
#define J8D *result = _client->read();
#define J8C } else if (!connected()) {
#define J8B for (i=0;i<plength;i++) {
#define J8A const char* idp = string;
#define J89 if (buffer[0]&MQTTQOS1) {
#define J88 len = plength + 2 + tlen;
#define J87 _client->write(buffer,2);
#define J86 _client->write(buffer,4);
#define J85 buffer[0] = MQTTPINGRESP;
#define J84 buffer[2] = (msgId >> 8);
#define J83 buf[pos-i-2] = (i >> 8);
#define J82 _state = MQTT_CONNECTED;
#define J81 this->_client = &client;
#define J80 pingOutstanding = false;
#define J7F buffer[length++] = d[j];
#define J7E buffer[0] = MQTTPINGREQ;
#define J7D uint32_t multiplier = 1;
#define J7C buffer[length++] = qos;
#define J7B buffer[pos++] = header;
#define J7A setServer(domain,port);
#define J79 if (_client == NULL ) {
#define J78 buffer[0] = MQTTPUBACK;
#define J77 this->stream = &stream;
#define J76 return this->debug_var;
#define J75 pingOutstanding = true;
#define J74 this->domain = domain;
#define J73 buffer[pos++] = digit;
#define J72 if (pingOutstanding) {
#define J71 lenBuf[pos++] = digit;
#define J70 setCallback(callback);
#define J6F setServer(addr, port);
#define J6E *lengthLength = len-1;
#define J6D uint16_t len = length;
#define J6C boolean result = true;
#define J6B buffer[len++] = digit;
#define J6A buffer[llen+2+tl] = 0;
#define J69 bytesRemaining -= rc;
#define J68 buffer[length++] = v;
#define J67 unsigned int pos = 0;
#define J66 if (buffer[3] == 0) {
#define J65 this->_client = NULL;
#define J64 buf[4-llen] = header;
#define J63 if (domain != NULL) {
#define J62 if (nextMsgId == 0) {
#define J61 uint8_t bytesToWrite;
#define J60 setServer(addr,port);
#define J5F tlen = strlen(topic);
#define J5E header = MQTTPUBLISH;
#define J5D buffer[len] = digit;
#define J5C unsigned int rc = 0;
#define J5B uint16_t length = 0;
#define J5A this->domain = NULL;
#define J59 return this->_state;
#define J58 return this->buffer;
#define J57 uint16_t length = 5;
#define J56 buf[pos++] = *idp++;
#define J55 lastOutActivity = t;
#define J54 setServer(ip, port);
#define J53 this->stream = NULL;
#define J52 debug_var = 4-llen;
#define J51 uint16_t msgId = 0;
#define J50 if (this->stream) {
#define J4F _state = buffer[3];
#define J4E setServer(ip,port);
#define J4D lastInActivity = t;
#define J4C if (!connected()) {
#define J4B multiplier *= 128;
#define J4A setCallback(NULL);
#define J49 setClient(client);
#define J48 setStream(stream);
#define J47 if(user != NULL) {
#define J46 this->port = port;
#define J45 if(pass != NULL) {
#define J44 uint16_t skip = 0;
#define J43 if (result == 1) {
#define J42 uint8_t lenBuf[4];
#define J41 uint8_t start = 0;
#define J40 if (connected()) {
#define J3F digit = len % 128;
#define J3E uint8_t digit = 0;
#define J3D _client->flush();
#define J3C uint8_t llen = 0;
#define J3B uint16_t len = 0;
#define J3A unsigned int len;
#define J39 uint8_t *payload;
#define J38 len = len / 128;
#define J37 uint8_t pos = 0;
#define J36 _client->stop();
#define J35 if (willTopic) {
#define J34 if (isPublish) {
#define J33 v = v|(0x80>>1);
#define J32 if (retained) {
#define J31 } while(len>0);
#define J30 if (len == 4) {
#define J2F uint8_t header;
#define J2E int result = 0;
#define J2D unsigned int i;
#define J2C unsigned int j;
#define J2B uint16_t i = 0;
#define J2A if (len == 6) {
#define J29 writeBuf += rc;
#define J28 if (callback) {
#define J27 if (len > 0) {
#define J26 digit |= 0x80;
#define J25 return result;
#define J24 buffer[1] = 2;
#define J23 buffer[1] = 0;
#define J22 this->ip = ip;
#define J21 nextMsgId = 1;
#define J20 uint8_t digit;
#define J1F while (*idp) {
#define J1E dump_buffer();
#define J1D uint16_t tlen;
#define J1C if (qos > 1) {
#define J1B uint8_t llen;
#define J1A return *this;
#define J19 return false;
#define J18 return true;
#define J17 nextMsgId++;
#define J16 uint16_t rc;
#define J15 header |= 1;
#define J14 boolean rc;
#define J13 uint16_t i;
#define J12 rc = false;
#define J11 v = v|0x80;
#define J10 return pos;
#define JF return len;
#define JE uint8_t v;
#define JD if (!rc) {
#define JC return rc;
#define JB start = 2;
#define JA skip += 2;
#define J9 v = 0x02;
#define J8 return 0;
#define J7 pos += 2;
#define J6 len = 0;
#define J5 } else {
#define J4 llen++;
#define J3 len++;
#define J2 i++;
#define J1 do {
#define J0 }
#include "LNSF_AIoT_PubSubClient.h"
#include "Arduino.h"
#define J123 JAB JB4 J65 J53 J4A J0 JDF JB4 J49 J53
#define J124 J0 J10A JB4 J6F J49 J53 J0 J113 JB4 J60
#define J125 J49 J48 J0 J118 JB4 J6F J70 J49 J53 J0
#define J126 J11E JB4 J60 J70 J49 J48 J0 J109 JB4 J54
#define J127 J49 J53 J0 J111 JB4 J4E J49 J48 J0 J117
#define J128 JB4 J54 J70 J49 J53 J0 J11C JB4 J4E J70
#define J129 J49 J48 J0 J10E JB4 J7A J49 J53 J0 J115
#define J12A JB4 J7A J49 J48 J0 J11A JB4 J7A J70 J49
#define J12B J53 J0 J120 JB4 J7A J70 J49 J48 J0 JE4
#define J12C JCA J0 J10F JC6 J0 J121 J106 J0 J122 J4C
#define J12D J2E J63 JF1 J5 JE6 J0 J43 J21 J57 J2C
#define J12E J123 J124 J125 J126 J127 J128 J129 J12A J12B J12C
#define J12F J12D 
#define J130 J12E J12F 
#define J131(__FOX__) __FOX__
J131(J130)
#if MQTT_VERSION == MQTT_VERSION_3_1
#define J132 J100 
#define J133(__FOX__) __FOX__
J133(J132)
#define MQTT_HEADER_VERSION_LENGTH 9
#elif MQTT_VERSION == MQTT_VERSION_3_1_1
#define J134 JF8 
#define J135(__FOX__) __FOX__
J135(J134)
#define MQTT_HEADER_VERSION_LENGTH 7
#endif
#define J136 JE1 J7F J0 JE J35 JCC J5 J9 J0 J47
#define J137 J11 J45 J33 J0 J0 J68 JDA JE0 JCF J35
#define J138 JE2 JE7 J0 J47 JD6 J45 JD7 J0 J0 JBC
#define J139 JDE JAE J97 JFE JB9 J36 J19 J0 J0 J1B
#define J13A JB8 J30 J66 J90 J80 J82 J18 J5 J4F J0
#define J13B J0 J36 J5 JA5 J0 J19 J0 J18 J0 JEE
#define J13C JBE JA8 JBA J10D J19 J0 J0 J8D J18 J0
#define J13D J102 JB0 JF0 JA0 J9B J18 J0 J19 J0 JF9
#define J13E J3B JC5 JE9 J7D J5B J3E J44 J41 J1 J2A
#define J13F J9D J36 J8 J0 JAD J6B JC7 J4B JA3 J6E
#define J140 J34 JC5 JC5 JFB JB J89 JA J0 J0 JD1
#define J141 JAD J50 JDD J95 J0 J0 JB3 J5D J0 J3
#define J142 J0 JEC J6 J0 JF J0 JA9 J40 J97 J119
#define J143 J72 JCE J36 J19 J5 J7E J23 J87 J55 J4D
#define J144 J75 J0 J0 J9E J1B JB8 J51 J39 J27 J4D
#define J145 JA6 J91 J28 JEB JD2 J6A JC1 JBF JEF JA4
#define J146 JD4 J78 J24 J84 J9C J86 J55 J5 J9A JCD
#define J147 J0 J0 JB2 J85 J23 J87 JBB J80 J0 J8C
#define J148 J19 J0 J0 J18 J0 J19 J0 J108 J105 J0
#define J149 J112 J107 J0 J116 JE3 J0 J11B JFD J0 J11D
#define J14A J40 JFA J19 J0 J57 JD8 J13 J8B JAA J0
#define J14B JA1 J32 J15 J0 JC8 J0 J19 J0 J11F J3C
#define J14C J20 J5C J1D J67 J2D J2F J3A J4C J19 J0
#define J14D J5F J5E J32 J15 J0 J7B J88 J1 J3F J38
#define J14E J27 J26 J0 J73 J4 J31 JC0 JB6 J8B JFC
#define J14F J0 J98 JAF J0 J10C J42 J3C J20 J37 J16
#define J150 J6D J1 J3F J38 J27 J26 J0 J71 J4 J31
#define J151 J64 J8F J93 J0 J1E 
#define J152 J136 J137 J138 J139 J13A J13B J13C J13D J13E J13F
#define J153 J140 J141 J142 J143 J144 J145 J146 J147 J148 J149
#define J154 J14A J14B J14C J14D J14E J14F J150 J151 
#define J155 J152 J153 J154 
#define J156(__FOX__) __FOX__
J156(J155)
#ifdef MQTT_MAX_TRANSFER_SIZE
#define J157 JB7 JD3 J61 J6C JD0 J114 JDC JA7 J69 J29
#define J158 J0 J25 
#define J159 J157 J158 
#define J15A(__FOX__) __FOX__
J15A(J159)
#else
#define J15B J52 JE8 J98 JA2 
#define J15C(__FOX__) __FOX__
J15C(J15B)
#endif
#define J15D J0 JF2 J99 J0 JFF J1C J19 J0 JE5 J19
#define J15E J0 J40 J57 J17 J62 J21 J0 JC4 JCB JED
#define J15F J7C JF3 J0 J19 J0 JF4 JE5 J19 J0 J40
#define J160 J57 J17 J62 J21 J0 JC4 JCB JDB JF7 J0
#define J161 J19 J0 JB5 J94 J23 J87 J9D J36 JDE J0
#define J162 J110 J8A J2B J7 J1F J56 J2 J0 J83 J92
#define J163 J10 J0 JBD J14 J79 J12 J5 JAC JD JC9
#define J164 JC3 J3D J36 J0 J0 J0 JC J0 J103 JD5
#define J165 J9F J0 J104 J22 J46 J5A J1A J0 J10B J74
#define J166 J46 J1A J0 J101 J8E J1A J0 JF5 J81 J1A
#define J167 J0 JF6 J77 J1A J0 J96 J59 J0 JD9 J58
#define J168 J0 JB1 JEA J0 JC2 J76 J0 
#define J169 J15D J15E J15F J160 J161 J162 J163 J164 J165 J166
#define J16A J167 J168 
#define J16B J169 J16A 
#define J16C(__FOX__) __FOX__
J16C(J16B)