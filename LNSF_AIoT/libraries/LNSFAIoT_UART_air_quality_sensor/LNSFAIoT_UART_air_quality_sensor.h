/***************************************************
  Written by HuangHuijuan from LNSF_AIoT on 2022/07/14.
 ****************************************************/
#ifndef _LNSFAIOT_UART_AIR_QUALITY_SERSOR
#define _LNSFAIOT_UART_AIR_QUALITY_SERSOR

#include "Arduino.h"


#if ARDUINO >= 100
#define SERIAL_WRITE(...) mySerial->write(__VA_ARGS__)
#else
#define SERIAL_WRITE(...) mySerial->write(__VA_ARGS__, BYTE)
#endif

#define SERIAL_WRITE_U16(v)        \
  SERIAL_WRITE((uint8_t)(v >> 8)); \
  SERIAL_WRITE((uint8_t)(v & 0xFF));

class LNSFAIoT_UART_air_quality_sensor
{

public:
  void begin(Stream *serial);

void LNSFAIoT_air_quality_sensor_UARTanalysis(void);
void LNSFAIoT_air_quality_sensor_UARTprintln(void);
float LNSFAIoT_air_num_ShiDu_UARTfunc(void);
float LNSFAIoT_air_num_WenDu_UARTfunc(void);
float LNSFAIoT_air_num_PM10_UARTfunc(void);
float LNSFAIoT_air_num_CO2_UARTfunc(void);
float LNSFAIoT_air_num_CH2O_UARTfunc(void);
float LNSFAIoT_air_num_TVOC_UARTfunc(void);
float LNSFAIoT_air_num_PM25_UARTfunc(void);

private:

  Stream *mySerial;
  
};
#endif
