
#define B2A LNSFAIoT_air_num_ShiDu = ((String(LNSFAIoT_list_SerialData[12]).toFloat()) * 256 + (String(LNSFAIoT_list_SerialData[13]).toFloat())) * 0.1;
#define B29 LNSFAIoT_air_num_WenDu = ((String(LNSFAIoT_list_SerialData[10]).toFloat()) * 256 + (String(LNSFAIoT_list_SerialData[11]).toFloat())) * 0.1;
#define B28 LNSFAIoT_air_num_PM10 = (String(LNSFAIoT_list_SerialData[8]).toFloat()) * 256 + String(LNSFAIoT_list_SerialData[9]).toFloat();
#define B27 LNSFAIoT_air_num_PM25 = (String(LNSFAIoT_list_SerialData[6]).toFloat()) * 256 + String(LNSFAIoT_list_SerialData[7]).toFloat();
#define B26 unsigned int LNSFAIoT_air_num_PM10, LNSFAIoT_air_num_CO2, LNSFAIoT_air_num_CH2O, LNSFAIoT_air_num_TVOC, LNSFAIoT_air_num_PM25;
#define B25 LNSFAIoT_air_num_CO2 = (String(LNSFAIoT_list_SerialData[0]).toFloat()) * 256 + String(LNSFAIoT_list_SerialData[1]).toFloat();
#define B24 LNSFAIoT_air_num_CH2O = (String(LNSFAIoT_list_SerialData[2]).toInt()) * 256 + String(LNSFAIoT_list_SerialData[3]).toInt();
#define B23 LNSFAIoT_air_num_TVOC = (String(LNSFAIoT_list_SerialData[4]).toInt()) * 256 + String(LNSFAIoT_list_SerialData[5]).toInt();
#define B22 void LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_quality_sensor_UARTanalysis(void)
#define B21 void LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_quality_sensor_UARTprintln(void)
#define B20 float LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_num_WenDu_UARTfunc(void)
#define B1F float LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_num_ShiDu_UARTfunc(void)
#define B1E float LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_num_TVOC_UARTfunc(void)
#define B1D float LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_num_CH2O_UARTfunc(void)
#define B1C float LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_num_PM10_UARTfunc(void)
#define B1B float LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_num_PM25_UARTfunc(void)
#define B1A float LNSFAIoT_UART_air_quality_sensor::LNSFAIoT_air_num_CO2_UARTfunc(void)
#define B19 Serial.println("PM2.5: " + String(LNSFAIoT_air_num_PM25) + " ug/m3");
#define B18 Serial.println("温度: " + String(LNSFAIoT_air_num_WenDu) + " ℃");
#define B17 Serial.println("CH2O: " + String(LNSFAIoT_air_num_CH2O) + " ug/m3");
#define B16 Serial.println("PM10: " + String(LNSFAIoT_air_num_PM10) + " ug/m3");
#define B15 Serial.println("TVOC: " + String(LNSFAIoT_air_num_TVOC) + " ug/m3");
#define B14 Serial.println("湿度: " + String(LNSFAIoT_air_num_ShiDu) + " %");
#define B13 Serial.println("CO2: " + String(LNSFAIoT_air_num_CO2) + " ppm");
#define B12 void LNSFAIoT_UART_air_quality_sensor::begin(Stream *serial)
#define B11 if ((mySerial->read() == 60) && (mySerial->read() == 2))
#define B10 float LNSFAIoT_air_num_WenDu, LNSFAIoT_air_num_ShiDu;
#define BF LNSFAIoT_list_SerialData[j] = mySerial->read();
#define BE int LNSFAIoT_list_SerialData[14];
#define BD if (mySerial->available() > 0)
#define BC return LNSFAIoT_air_num_ShiDu;
#define BB return LNSFAIoT_air_num_WenDu;
#define BA return LNSFAIoT_air_num_CH2O;
#define B9 return LNSFAIoT_air_num_PM25;
#define B8 return LNSFAIoT_air_num_TVOC;
#define B7 return LNSFAIoT_air_num_PM10;
#define B6 return LNSFAIoT_air_num_CO2;
#define B5 for (int j = 0; j < 14; j++)
#define B4 mySerial = serial;
#define B3 delay(1000);
#define B2 delay(500);
#define B1 {
#define B0 }
#include <LNSFAIoT_UART_air_quality_sensor.h>
#define B2B BE B26 B10 B12 B1 B3 B4 B0 B22 B1
#define B2C BD B1 B11 B1 B5 B1 BF B0 B25 B24
#define B2D B23 B27 B28 B29 B2A B0 B0 B2 B0 B21
#define B2E B1 B14 B18 B16 B13 B17 B15 B19 B0 B1F
#define B2F B1 BC B0 B20 B1 BB B0 B1C B1 B7
#define B30 B0 B1A B1 B6 B0 B1D B1 BA B0 B1E
#define B31 B1 B8 B0 B1B B1 B9 B0 
#define B32 B2B B2C B2D B2E B2F B30 B31 
#define B33(__FOX__) __FOX__
B33(B32)