#ifndef _TOKEN_H_
#define _TOKEN_H_

#include "base64.h"
#include "md5.h"

#include <stdio.h>
#include <string.h>

typedef  void (*iapfun)(void); 			


typedef struct
{
	char authorization[128];			
	char version[8];					
	char token[32];						
	unsigned int size;					
	char md5[40];						
	unsigned int addr;					
	unsigned char ota_start : 1;		
	unsigned char ota_check : 1;		
	unsigned char ota_download_ok : 1;	
	unsigned char ota_report_ok : 1;	
	unsigned char reverse : 4;

} OTA_INFO;

extern OTA_INFO ota_info;

class LNSF_AIoT_MQTT_TOKEN
{
public:
unsigned char TOKEN_Authorization(char *ver, char *res, unsigned int et, char *access_key, char *token_buf, unsigned short token_buf_len);
};
#endif
