
#define J14F xTaskCreatePinnedToCore(_executeCallbackTask, "_executeCallbackTask", 2048, mqttPt, 1, &mqttPt->iotDebug.task, ARDUINO_RUNNING_CORE_MQTT);
#define J14E mqttPt->iotDebug.message = "The server doesn’t support the requested version of MQTT, please check the parameters!";
#define J14D MyHmac_Sha1.HMAC_SHA1((byte * )tempData,Data.length(),(byte * )tempSecret,this->_DeviceSecret.length(),tempPassWord);
#define J14C mqttPt->iotDebug.message = "Unable to access the set SSID network [" + mqttPt->account + "], Wifi password error.";
#define J14B mqttPt->iotDebug.message = "Unable to access the set SSID network [" + mqttPt->account + "], Wifi does not exist.";
#define J14A mqttPt->iotDebug.message = "The server didn’t respond within the keepalive time, please check the parameters!";
#define J149 if(mqttPt->iot_debugMsgHandleCb) mqttPt->iot_debugMsgHandleCb(self->iotDebug.message, self->iotDebug.error);
#define J148 mqttPt->iotDebug.message = "The server was unable to accept the connection, please check the parameters!";
#define J147 void LNSF_AIoT_MQTT::setMqttCallback(Topicnum topic_num, const String delimiters, const HandleCb handles){
#define J146 xTaskCreatePinnedToCore(iotDebugTask, "iotDebugTask", 2048, this, 1, NULL, ARDUINO_RUNNING_CORE_MQTT);
#define J145 mqttPt->iotDebug.message = "The server rejected the client identifier, please check the parameters!";
#define J144 mqttPt->iotDebug.message = "The client was not authorized to connect! please check the parameters!";
#define J143 xTaskCreatePinnedToCore(mqttTask, "mqttTask", 2048, mqttPt, 1, NULL, ARDUINO_RUNNING_CORE_MQTT);
#define J142 mqttPt->iotDebug.message = "The username/password were rejected, please check the parameters!";
#define J141 return currentWifiStatus == iotStatusProtocol::WifiConnected && WiFi.status() == WL_CONNECTED;
#define J140 mqttPt->iotDebug.message = "The network connection was broken, please check the parameters!";
#define J13F mqttPt->iotDebug.message = "The network connection failed! Please check the parameters!";
#define J13E mqttPt->iotDebug.message = "Wifi connect to [" + WiFi.SSID() + "] successfully" + "\n" +
#define J13D return currentMqttStatus == iotStatusProtocol::MqttConnected && client.connected();
#define J13C while(!client.connect(mqttPt->_clientId,mqttPt->_username,mqttPt->_password)) {
#define J13B token.TOKEN_Authorization("2018-10-31",buf1,1767077900,buf2,authorization,128);
#define J13A void LNSF_AIoT_MQTT::setMQTTDebugCallback(const DebugMsgHandleCb handleCb){
#define J139 static void mqttCallback(char * topic, byte * payload, unsigned int len){
#define J138 client.publish(this->mqtt_topicArray[topic_num].c_str(),message.c_str());
#define J137 void LNSF_AIoT_MQTT::publish(Topicnum topic_num, double f, int precision)
#define J136 mqttPt->iotDebug.message = "The client was not authorized to connect!";
#define J135 void LNSF_AIoT_MQTT::publish(Topicnum topic_num, const String& message)
#define J134 mqttPt->iotDebug.message = "Start connecting to " + mqttPt->account;
#define J133 void LNSF_AIoT_MQTT::init(String AliyunServer, String AliProductKey,
#define J132 mqttPt->iotDebug.message = String("Mqtt connect successfully\n") +
#define J131 String tempSERVER = (this->_ProductKey + "." + this->_MQTTSERVER);
#define J130 String tempPass = byteToHexStr(tempPassWord,sizeof(tempPassWord));
#define J12F void LNSF_AIoT_MQTT::wifiConnect(String account, String password){
#define J12E void LNSF_AIoT_MQTT::setMqttCallback(const MsgHandleCb handles[]){
#define J12D mqttPt->iotDebug.message = "The client is disconnected cleanly!";
#define J12C if(millis() - timeOut > 15000 && (!connected() || !wifiStatus()))
#define J12B String tempName = (this->_DeviceName + "&" + this->_ProductKey);
#define J12A mqttPt->iotDebug.message = "The network connection was broken!";
#define J129 mqttPt->iotDebug.message = "The network connection failed!";
#define J128 boolean LNSF_AIoT_MQTT::setSoftAP(String ssid, String pass)
#define J127 void LNSF_AIoT_MQTT::publish(Topicnum topic_num, int64_t i)
#define J126 this->_ApiKey.toCharArray(buf2, this->_ApiKey.length()+1);
#define J125 String byteToHexStr(unsigned char byte_arr[], int arr_len)
#define J124 mqttPt->iotDebug.error = IOT_MQTT_CONNECT_BAD_CREDENTIALS;
#define J123 for(int j = 0; j < mqttPt->mqtt_handleCb[i].size(); j++){
#define J122 mqttPt->iotDebug.error = IOT_MQTT_CONNECT_BAD_CLIENT_ID;
#define J121 for(int i = 0; i < mqttPt->mqtt_topicArray.size(); i++){
#define J120 currentWifiStatus = iotStatusProtocol::WifiConnectStart;
#define J11F currentMqttStatus = iotStatusProtocol::MqttConnectStart;
#define J11E mqttPt->iotDebug.error = IOT_MQTT_CONNECT_UNAUTHORIZED;
#define J11D volatile static bool firstRegisteredMQTTDebugtCallback;
#define J11C mqttPt->iotDebug.error = IOT_MQTT_CONNECT_BAD_PROTOCOL;
#define J11B currentWifiStatus = iotStatusProtocol::WifiConnecting;
#define J11A if(!wifiStatus() || !connected() || mqttCallbackbusyi)
#define J119 currentWifiStatus = iotStatusProtocol::WifiConnectEnd;
#define J118 " [ Server: " + String(mqttPt->_mqttServer) + " ]\n" +
#define J117 mqttPt->iotDebug.error = IOT_MQTT_CONNECT_UNAVAILABLE;
#define J116 currentMqttStatus = iotStatusProtocol::MqttConnectEnd;
#define J115 currentMqttStatus = iotStatusProtocol::MqttConnecting;
#define J114 currentWifiStatus = iotStatusProtocol::WifiConnected;
#define J113 mqttPt->iotDebug.error = IOT_MQTT_CONNECTION_TIMEOUT;
#define J112 this->_password = (char *) malloc(tempPass.length());
#define J111 mqttPt->iotDebug.message = "Wifi connection failed!";
#define J110 client.subscribe(mqttPt->mqtt_topicArray[i].c_str());
#define J10F currentMqttStatus = iotStatusProtocol::MqttConnected;
#define J10E }else if(mqttStatus == MQTT_CONNECT_BAD_CREDENTIALS){
#define J10D " [ NetMask: " + mqttPt->getWiFiNetmask() + " ]\n" +
#define J10C mqttPt->iotDebug.message = "Wifi lost connection!";
#define J10B mqttPt->iotDebug.message = "Start connecting mqtt";
#define J10A }else if(mqttStatus == MQTT_CONNECT_BAD_CLIENT_ID){
#define J109 currentMqttStatus = iotStatusProtocol::NoneStatus;
#define J108 mqttPt->iotDebug.error = IOT_MQTT_CONNECTION_LOST;
#define J107 currentWifiStatus = iotStatusProtocol::NoneStatus;
#define J106 }else if(mqttStatus == MQTT_CONNECT_BAD_PROTOCOL){
#define J105 }else if(mqttStatus == MQTT_CONNECT_UNAUTHORIZED){
#define J104 mqtt_delimiters[topic_num].push_back(delimiters);
#define J103 }else if(mqttStatus == MQTT_CONNECT_UNAVAILABLE){
#define J102 mqttPt->iotDebug.error = IOT_MQTT_CONNECT_FAILED;
#define J101 String AliDeviceSecret, const String iotTopics[],
#define J100 volatile static bool firstRegisteredMqttCallback;
#define JFF mqttPt->iotDebug.error = IOT_WL_CONNECTION_LOST;
#define JFE client.setServer(this->_mqttServer,this->_port);
#define JFD " [ GateWay: " + mqttPt->getGateway() + " ]\n" +
#define JFC mqttPt->iotDebug.error = IOT_MQTT_DISCONNECTED;
#define JFB char *p = (char *)(str.c_str() + str.length());
#define JFA mqttPt->iotDebug.error = IOT_WL_CONNECT_FAILED;
#define JF9 " [ IP: " + mqttPt->getWiFiLocalIP() + " ]\n" +
#define JF8 LNSF_AIoT_MQTT *self = (LNSF_AIoT_MQTT *)param;
#define JF7 strcpy(tempSecret,this->_DeviceSecret.c_str());
#define JF6 if(topic_num >= mqtt_topicArray.size()) return;
#define JF5 if(millis() - timeOut > 10000 && !wifiStatus())
#define JF4 String OneNetApiKey, const String iotTopics[],
#define JF3 this->mqtt_topicArray.push_back(iotTopics[i]);
#define JF2 " [ ClientId: " + mqttPt->_clientId + " ]\n" +
#define JF1 mqttPt->iotDebug.message = "Wifi disconnect!";
#define JF0 char tempSecret[this->_DeviceSecret.length()];
#define JEF void LNSF_AIoT_MQTT::init(String OneNetServer,
#define JEE if(_payload == mqttPt->mqtt_delimiters[i][j]){
#define JED static void _executeCallbackTask(void *param){
#define JEC " [ Username: " + mqttPt->_username + " ]\n" +
#define JEB String OneNetProductID, String OneNetDeviceID,
#define JEA return WiFi.softAP(ssid.c_str(),pass.c_str());
#define JE9 mqttPt->iotDebug.error = IOT_WL_NO_SSID_AVAIL;
#define JE8 strcpy(this->_mqttServer,tempSERVER.c_str());
#define JE7 mqttPt->iotDebug.error = IOT_WL_DISCONNECTED;
#define JE6 WiFi.begin(account.c_str(),password.c_str());
#define JE5 String Data = ("clientId" + this->_ClientId +
#define JE4 }else if(mqttStatus == MQTT_CONNECTION_LOST){
#define JE3 mqtt_handleCb[topic_num].push_back(handles);
#define JE2 }else if(mqttStatus == MQTT_CONNECT_FAILED){
#define JE1 }else if(wifiStatus == WL_CONNECTION_LOST) {
#define JE0 if(mqttStatus == MQTT_CONNECTION_TIMEOUT) {
#define JDF " [ Password: " + mqttPt->_password + " ]";
#define JDE }else if(wifiStatus == WL_CONNECT_FAILED) {
#define JDD publish(topic_num, (const String&)buffer);
#define JDC }else if(wifiStatus == WL_NO_SSID_AVAIL) {
#define JDB firstRegisteredMQTTDebugtCallback = false;
#define JDA sprintf(myaddr,"%d.%d.%d.%d",a4,a3,a2,a1);
#define JD9 mqttPt->iotDebug.error = IOT_SUCCESSFULLY;
#define JD8 }else if(mqttStatus == MQTT_DISCONNECTED){
#define JD7 strcpy(this->_username,tempName.c_str());
#define JD6 strcpy(this->_password,tempPass.c_str());
#define JD5 volatile static int8_t currentWifiStatus;
#define JD4 this->_mqttServer = (char *) malloc(len);
#define JD3 String AliClientId, String AliDeviceName,
#define JD2 case iotStatusProtocol::MqttConnectStart:
#define JD1 }else if(wifiStatus == WL_DISCONNECTED) {
#define JD0 volatile static int8_t currentMqttStatus;
#define JCF case iotStatusProtocol::WifiConnectStart:
#define JCE if(_topic == mqttPt->mqtt_topicArray[i]){
#define JCD firstRegisteredMQTTDebugtCallback = true;
#define JCC }else if(this->_UseServer == ONENET_NEW){
#define JCB for(int i = 0; i < MAXTOPICNUMBER; i++){
#define JCA unsigned  int IP_Addr = WiFi.softAPIP();
#define JC9 if(mqttPt->mqtt_topicArray[i] != "$dp"){
#define JC8 String tempPass = String(authorization);
#define JC7 this->_username = (char * )malloc(len);
#define JC6 this->mqtt_msgHandleCb[i] = handles[i];
#define JC5 String LNSF_AIoT_MQTT::getWiFiLocalIP()
#define JC4 case iotStatusProtocol::MqttConnectEnd:
#define JC3 }else if(mqttStatus == MQTT_CONNECTED){
#define JC2 case iotStatusProtocol::MqttConnecting:
#define JC1 case iotStatusProtocol::WifiConnectEnd:
#define JC0 case iotStatusProtocol::WifiConnecting:
#define JBF publish(topic_num, (const String&)str);
#define JBE this->_ProductID     = OneNetProductID;
#define JBD strcpy(this->_clientId,tempID.c_str());
#define JBC if(firstRegisteredMQTTDebugtCallback) {
#define JBB this->_DeviceSecret  = AliDeviceSecret;
#define JBA this->_clientId = (char *) malloc(len);
#define JB9 String LNSF_AIoT_MQTT::getWiFiNetmask()
#define JB8 if(mqttStatus == MQTT_CONNECTION_LOST){
#define JB7 volatile static bool mqttCallbackbusyi;
#define JB6 String tempSERVER = this->_MQTTSERVER;
#define JB5 static void iotDebugTask(void *param){
#define JB4 case iotStatusProtocol::MqttConnected:
#define JB3 case iotStatusProtocol::WifiConnected:
#define JB2 pid.toCharArray(buf1, pid.length()+1);
#define JB1 mqttPt->mqtt_msgHandleCb[i](_payload);
#define JB0 void LNSF_AIoT_MQTT::wifiDisconnect(){
#define JAF if(wifiStatus == WL_CONNECTION_LOST) {
#define JAE " [ Port: " + mqttPt->_port + " ]\n" +
#define JAD }else if(wifiStatus == WL_CONNECTED) {
#define JAC this->_DeviceID      = OneNetDeviceID;
#define JAB LNSF_AIoT_MQTT::~LNSF_AIoT_MQTT(void){
#define JAA String LNSF_AIoT_MQTT::getWiFiSoftIP()
#define JA9 _payload = _payload.substring(0,len);
#define JA8 " [ DNS: " + mqttPt->getDNS() + " ]";
#define JA7 this->_DeviceName    = AliDeviceName;
#define JA6 while(!connected() || !wifiStatus()){
#define JA5 boolean LNSF_AIoT_MQTT::wifiStatus(){
#define JA4 }else if(this->_UseServer == ALIYUN){
#define JA3 this->_ProductKey    = AliProductKey;
#define JA2 ",timestamp="+(String)timestamp+"|");
#define JA1 LNSF_AIoT_MQTT::LNSF_AIoT_MQTT(void){
#define JA0 this->_MQTTSERVER    = OneNetServer;
#define J9F firstRegisteredMqttCallback = false;
#define J9E this->_ApiKey        = OneNetApiKey;
#define J9D if(wifiStatus == WL_NO_SSID_AVAIL) {
#define J9C return WiFi.subnetMask().toString();
#define J9B this->_MQTTSERVER    = AliyunServer;
#define J9A if(OneNetServer == "183.230.40.96"){
#define J99 boolean LNSF_AIoT_MQTT::connected(){
#define J98 char buf2[this->_ApiKey.length()+1];
#define J97 String LNSF_AIoT_MQTT::getGateway()
#define J96 vTaskDelete(mqttPt->iotDebug.task);
#define J95 firstRegisteredMqttCallback = true;
#define J94 this->_ClientId      = AliClientId;
#define J93 String tempName = this->_ProductID;
#define J92 return WiFi.gatewayIP().toString();
#define J91 static void mqttTask(void *param){
#define J90 void LNSF_AIoT_MQTT::disconnect(){
#define J8F uint8_t len = tempSERVER.length();
#define J8E this->_UseServer     = ONENET_NEW;
#define J8D String _payload = (char *)payload;
#define J8C String tempID = (this->_ClientId +
#define J8B if(mqttPt->iot_debugMsgHandleCb) {
#define J8A "productKey" + this->_ProductKey +
#define J89 this->_port          = OneNetPort;
#define J88 "deviceName" + this->_DeviceName +
#define J87 if(firstRegisteredMqttCallback) {
#define J86 void LNSF_AIoT_MQTT::setConfig(){
#define J85 return WiFi.localIP().toString();
#define J84 "timestamp" + (String)timestamp);
#define J83 static void executeCallbackTask()
#define J82 client.setCallback(mqttCallback);
#define J81 String tempPass = this->_ApiKey;
#define J80 iot_debugMsgHandleCb = handleCb;
#define J7F String tempID = this->_DeviceID;
#define J7E if(this->_UseServer == ONENET){
#define J7D PubSubClient client(espClient);
#define J7C String LNSF_AIoT_MQTT::getDNS()
#define J7B if(mqttPt->mqtt_msgHandleCb[i])
#define J7A void LNSF_AIoT_MQTT::connect(){
#define J79 return WiFi.dnsIP().toString();
#define J78 this->_port          = AliPort;
#define J77 this->_UseServer     = ALIYUN;
#define J76 this->_UseServer     = ONENET;
#define J75 if(this->_mqttServer == NULL){
#define J74 String pid = this->_ProductID;
#define J73 while(*p == '\0'|| *p == '0'){
#define J72 strcpy(tempData,Data.c_str());
#define J71 mqttPt->mqtt_handleCb[i][j]();
#define J70 mqttPt->iotDebug.task = NULL;
#define J6F " [ Mode: " + mode + " ]\n" +
#define J6E boolean connectState = false;
#define J6D char tempData[Data.length()];
#define J6C if(millis() - timeOut > 3000)
#define J6B mqttStatus = client.state();
#define J6A ",signmethod=" + "hmacsha1"+
#define J69 if(this->_clientId == NULL){
#define J68 if(this->_password == NULL){
#define J67 while(!client.connected()){
#define J66 self->iotDebug.task = NULL;
#define J65 wifiStatus = WiFi.status();
#define J64 for (int i=0;i<arr_len;i++)
#define J63 LNSF_AIoT_MQTT_TOKEN token;
#define J62 mqttPt->iotDebug.error = 0;
#define J61 int wifiStatus, mqttStatus;
#define J60 if(mqttPt->iotDebug.task){
#define J5F char buf1[pid.length()+1];
#define J5E switch(currentWifiStatus){
#define J5D String myLocalIP = myaddr;
#define J5C mqttCallbackbusyi = false;
#define J5B switch(currentMqttStatus){
#define J5A this->password = password;
#define J59 mqttCallbackbusyi = true;
#define J58 static void subscribe(){
#define J57 char authorization[128];
#define J56 uint16_t timestamp = 49;
#define J55 hexstr=hexstr+hex1+hex2;
#define J54 long timeOut = millis();
#define J53 len = tempName.length();
#define J52 this->account = account;
#define J51 CHMAC_SHA1 MyHmac_Sha1;
#define J50 pid += this->_DeviceID;
#define J4F switch(WiFi.getMode()){
#define J4E LNSF_AIoT_MQTT *mqttPt;
#define J4D if(iotTopics[i] != ""){
#define J4C memset(myaddr,'\0',50);
#define J4B if(client.connected())
#define J4A int value=byte_arr[i];
#define J49 memset(buffer, 0, 34);
#define J48 byte tempPassWord[20];
#define J47 WiFi.disconnect(true);
#define J46 len = tempID.length();
#define J45 executeCallbackTask();
#define J44 String _topic = topic;
#define J43 this->password = pass;
#define J42 int a3 = IP_Addr>>24;
#define J41 IP_Addr = IP_Addr<<8;
#define J40 while(!wifiStatus()){
#define J3F case WIFI_MODE_APSTA:
#define J3E this->account = ssid;
#define J3D int a4 = IP_Addr>>24;
#define J3C WiFiClient espClient;
#define J3B int a2 = IP_Addr>>24;
#define J3A int a1 = IP_Addr>>24;
#define J39 case WIFI_MODE_NULL:
#define J38 client.disconnect();
#define J37 itoa(i, buffer, 10);
#define J36 uint16_t OneNetPort)
#define J35 case WIFI_MODE_STA:
#define J34 pid += "/devices/";
#define J33 hex1=(char)(48+v1);
#define J32 hex1=(char)(55+v1);
#define J31 timeOut = millis();
#define J30 hex2=(char)(48+v2);
#define J2F hex2=(char)(55+v2);
#define J2E vTaskDelete(NULL);
#define J2D str = String(f,5);
#define J2C int v2=value % 16;
#define J2B case WIFI_MODE_AP:
#define J2A return myLocalIP;
#define J29 wifiDisconnect();
#define J28 uint16_t AliPort)
#define J27 if (v2>=0&&v2<=9)
#define J26 if (v1>=0&&v1<=9)
#define J25 int v1=value/16;
#define J24 "|securemode=3"+
#define J23 char myaddr[50];
#define J22 char buffer[34];
#define J21 mode = "APSTA";
#define J20 if(handles[i]){
#define J1F if(str == "-0")
#define J1E String hexstr;
#define J1D mode = "NULL";
#define J1C mqttPt = NULL;
#define J1B client.loop();
#define J1A mqttPt = this;
#define J19 return hexstr;
#define J18 long timeOut;
#define J17 if(*p == '.')
#define J16 mode = "STA";
#define J15 mode = "AP";
#define J14 subscribe();
#define J13 String mode;
#define J12 setConfig();
#define J11 delay(100);
#define J10 String str;
#define JF delay(200);
#define JE *p = '\0';
#define JD delay(10);
#define JC char hex2;
#define JB delay(50);
#define JA char hex1;
#define J9 str = "0";
#define J8 while(1){
#define J7 yield();
#define J6 return;
#define J5 }else{
#define J4 break;
#define J3 else
#define J2 p--;
#define J1 {
#define J0 }
#include "LNSF_AIoT_MQTT.h"
#include "HMAC_SHA1.h"
#include "token.h"
#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE_MQTT 0
#else
#define ARDUINO_RUNNING_CORE_MQTT 1
#endif
#define J150 J51 J4E J63 J11D J100 JB7 JD5 JD0 J125 J1
#define J151 J1E J64 J1 JA JC J4A J25 J2C J26 J33
#define J152 J3 J32 J27 J30 J3 J2F J55 J0 J19 J0
#define J153 J139 J59 J44 J121 JCE J8D JA9 J123 JEE J71
#define J154 J4 J0 J0 J7B JB1 J4 J0 J0 J5C J0
#define J155 J58 J121 JC9 J110 J0 J0 J0 J91 J67 J11
#define J156 J7 J0 J8 J67 J11 J7 J0 J1B J7 J0
#define J157 J0 JED JF8 J149 J66 J2E J0 J83 J1 J8B
#define J158 J60 J96 J70 J0 J14F J0 J0 JB5 J61 J18
#define J159 J8 J65 J6B J5E JCF J134 JD9 J11B J45 J4
#define J15A JC0 JAF J14C JFF J107 J45 JDC J14B JE9 J107
#define J15B J45 JAD J13 J4F J39 J1D J4 J35 J16 J4
#define J15C J2B J15 J4 J3F J21 J4 J0 J13E J6F JF9
#define J15D J10D JFD JA8 JD9 J119 J45 J0 J4 JC1 J114
#define J15E J4 JB3 J9D J47 J14B JE9 J107 J45 JDE J47
#define J15F J111 JFA J107 J45 JE1 J47 J10C JFF J107 J45
#define J160 JD1 J47 JF1 JE7 J107 J45 J0 J4 J0 J5B
#define J161 JD2 J10B J62 J115 J45 J31 J13C J6C J4 JD
#define J162 J0 J14 J87 J143 J9F J0 J4 JC2 JE0 J14A
#define J163 J113 J109 J45 JE4 J140 J108 J109 J45 JE2 J13F
#define J164 J102 J109 J45 JC3 J132 J118 JAE JF2 JEC JDF
#define J165 JD9 J116 J45 J106 J14E J11C J109 J45 J10A J145
#define J166 J122 J109 J45 J103 J148 J117 J109 J45 J10E J142
#define J167 J124 J109 J45 J105 J144 J11E J109 J45 J0 J4
#define J168 JC4 J10F J4 JB4 JB8 J12A J108 J109 J45 JD8
#define J169 J12D JFC J109 J45 JE2 J129 J102 J109 J45 J103
#define J16A J136 J117 J109 J45 J0 J4 J0 JB J7 J0
#define J16B J0 JA1 J1A JCD J95 J5C J109 J107 JBC J146
#define J16C JDB J0 J0 JAB JDB J9F J1C J29 J0 J12F
#define J16D J52 J5A J47 J11 J120 JE6 J0 JA5 J141 J0
#define J16E JB0 J4B J38 J47 J0 J7A J6E J54 J40 JF5
#define J16F J6 JD J0 JFE J82 J11F J0 J99 J13D J0
#define J170 JC5 J1 J85 J0 JB9 J1 J9C J0 J97 J1
#define J171 J92 J0 J7C J1 J79 J0 J128 J1 J3E J43
#define J172 JEA J0 JAA J1 JCA J3A J41 J3B J41 J42
#define J173 J41 J3D J23 J4C JDA J5D J2A J0 J90 J38
#define J174 J0 J135 J1 J11A J6 JF6 J138 JF J0 J137
#define J175 J1 J10 J2D JFB J73 JE J2 J0 J17 JE
#define J176 J1F J9 J54 JA6 J12C J6 JD J0 JBF J0
#define J177 J127 J1 J22 J49 J37 JDD J0 JEF JEB JF4
#define J178 J36 J1 J9A JA0 JBE JAC J9E J89 J8E J5
#define J179 JA0 JBE JAC J9E J89 J76 J0 JCB J4D JF3
#define J17A J0 J0 J12 J0 J133 JD3 J101 J28 J1 J9B
#define J17B JA3 J94 JA7 JBB J78 J77 JCB J4D JF3 J0
#define J17C J0 J12 J0 J13A J80 J0 J12E JCB J20 JC6
#define J17D J0 J0 J0 J147 JE3 J104 J0 J86 J7E JB6
#define J17E J8F J75 JD4 J0 JE8 J7F J46 J69 JBA J0
#define J17F JBD J93 J53 JC7 JD7 J81 J68 J112 J0 JD6
#define J180 JA4 J131 J8F J56 J75 JD4 J0 JE8 J8C J24
#define J181 J6A JA2 J46 J69 JBA J0 JBD JE5 J88 J8A
#define J182 J84 J48 JF0 J6D J12B J53 JC7 JD7 J72 JF7
#define J183 J14D J130 J68 J112 J0 JD6 JCC JB6 J8F J75
#define J184 JD4 J0 JE8 J7F J46 J69 JBA J0 JBD J93
#define J185 J53 JC7 JD7 J57 J74 J34 J50 J5F JB2 J98
#define J186 J126 J13B JC8 J68 J112 J0 JD6 J0 J0 J3C
#define J187 J7D 
#define J188 J150 J151 J152 J153 J154 J155 J156 J157 J158 J159
#define J189 J15A J15B J15C J15D J15E J15F J160 J161 J162 J163
#define J18A J164 J165 J166 J167 J168 J169 J16A J16B J16C J16D
#define J18B J16E J16F J170 J171 J172 J173 J174 J175 J176 J177
#define J18C J178 J179 J17A J17B J17C J17D J17E J17F J180 J181
#define J18D J182 J183 J184 J185 J186 J187 
#define J18E J188 J189 J18A J18B J18C J18D 
#define J18F(__FOX__) __FOX__
J18F(J18E)