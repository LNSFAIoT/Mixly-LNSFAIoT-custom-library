/***************************************************
  Written by HuangHuijuan from LNSF_AIoT on 2022/09/02.
 ****************************************************/
#ifndef _LNSFAIOT_SINGLESERIALMP3
#define _LNSFAIOT_SINGLESERIALMP3

#include "Arduino.h"

class LNSFAIoT_SingleSerialMP3
{
public:
  void MP3pin(int pin);
  void MP3mode(char modenum);
  void MP3Volume(int Volume);
  void MP3playList(int songnum);

private:
  void SendData(char addr);
};
#endif
