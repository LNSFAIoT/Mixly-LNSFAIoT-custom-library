/*!
 *@file LNSF_AIoT_TypeConversion.cpp
 *@brief Processing of serial data for AI intelligent offline speech recognition and speech synthesis.
 *@licence     The MIT License (MIT)
 *@Contact 微信公众号:人工智能素养教育共同体
 *@author foxzenith benxiaohai 
 *@version  V1.0
 *@date  2022-7-25
 *@https://gitee.com/LNSFAIoT/offline-speech-recognition-and-synthesis-modules
*/ 

#ifdef __cplusplus
extern "C"
{
#endif
#include <Arduino.h>

typedef unsigned char uint8_t;
char low2cap(char c);
void array2string(byte array[], unsigned int len, char buffer[]);
void Ascii2Hex(char *src, uint8_t *dest, int len);
void Int2Bytes(int num, unsigned char* bytes, int size);
void double2bytes(double data, byte bytes[]);
#ifdef __cplusplus
}
#endif