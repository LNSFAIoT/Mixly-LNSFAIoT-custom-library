#ifdef __cplusplus
extern "C"
{
#endif

#include <Arduino.h>

typedef unsigned char uint8_t;
char lowtocap(char c);
void array_to_string(byte array[], unsigned int len, char buffer[]);
void AsciiToHex(char *src, uint8_t *dest, int len);
void IntToBytes(int num, unsigned char* bytes, int size);
void doubletobytes(double data, byte bytes[]);
#ifdef __cplusplus
}
#endif