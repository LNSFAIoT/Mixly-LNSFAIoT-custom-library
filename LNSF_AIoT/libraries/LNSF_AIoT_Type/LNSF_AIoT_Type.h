/*!
 *@file LNSF_AIoT_Serial_SpeechSerial.cpp
 *@brief Processing of serial data for AI intelligent offline speech recognition and speech synthesis.
 *@licence     The MIT License (MIT)
 *@Contact 微信公众号:人工智能素养教育共同体
 *@author foxzenith benxiaohai 
 *@version  V1.0
 *@date  2022-7-25
 *@https://gitee.com/LNSFAIoT/offline-speech-recognition-and-synthesis-modules
*/ 

#ifdef __cplusplus
extern "C"
{
#endif
#include <Arduino.h>

typedef union {
  unsigned char uint8_t;
  double d_double;
  unsigned char d_ucs[8];
  unsigned long d_long;
}uart_param_t;
char lowtocap(char c);
void array_to_string(byte array[], unsigned int len, char buffer[]);
void AsciiToHex(char *src, uint8_t *dest, int len);
void IntToBytes(int num, unsigned char* bytes, int size);
void doubletobytes(double data, byte bytes[]);
void _float_to_double(uart_param_t* param);
#ifdef __cplusplus
}
#endif