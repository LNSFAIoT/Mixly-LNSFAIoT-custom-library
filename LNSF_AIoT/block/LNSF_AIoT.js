﻿(function () {

  'use strict';
  goog.provide('Blockly.Blocks.LNSF_AIoT');
  goog.require('Blockly.Blocks');
  var FieldMultilineInput;
  if (typeof Mixly === 'object') {
    FieldMultilineInput = Blockly.FieldMultilineInput;
  } else {
    FieldMultilineInput = Blockly.FieldTextArea;
  }
  Blockly.Blocks.LNSF_AIoTLOGO = {
    init: function () {
      this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("../../media/LNSFAIoTwechat.jpg", 120, 120, "*"));
      this.setColour(90);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.LED_init = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LED_display.png", 30, 30, "*"))
        .appendField("初始化LED点阵屏总宽:")
        .appendField(new Blockly.FieldTextInput("64"), "weight")
        .appendField("px 总长:")
        .appendField(new Blockly.FieldTextInput("64"), "hight")
        .appendField("px 级联个数(行*列):")
        .appendField(new Blockly.FieldTextInput("1"), "row")
        .appendField("*")
        .appendField(new Blockly.FieldTextInput("1"), "col")
        .appendField("级联方式:")
        .appendField(new Blockly.FieldDropdown([[{ "src": "../../media/LED_stright.png", "width": 50, "height": 30, "alt": "*" }, "func1"], [{ "src": "../../media/Bottom_left_UPserpentine_chain.png", "width": 50, "height": 30, "alt": "*" }, "func2"], [{ "src": "../../media/Top-right_DOWN_serpentine_chain.png", "width": 50, "height": 30, "alt": "*" }, "func3"], [{ "src": "../../media/Vertical_serpentine_chain.png", "width": 50, "height": 30, "alt": "*" }, "func4"]]), "func");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#999999");
      this.setTooltip("确定好屏幕大小、级联个数。");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.LED_zh = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LED_display.png", 30, 30, "*"))
        .appendField(new Blockly.FieldMultilineInput("岭师AIoT12!"), "zhdata");
      this.setOutput(true, null);
      this.setColour("#999999");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.input_data_module = {
    init: function () {
      this.appendValueInput("inputdata")
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LED_display.png", 30, 30, "*"))
        .setCheck(null)
        .appendField("英文/数字/符号传参显示");
      this.appendDummyInput()
        .appendField("位置起始点:x")
        .appendField(new Blockly.FieldTextInput("0"), "x")
        .appendField(" y")
        .appendField(new Blockly.FieldTextInput("0"), "y")
        .appendField("字号")
        .appendField(new Blockly.FieldTextInput("0"), "TextSize")
        .appendField("颜色")
        .appendField(new Blockly.FieldColour("#ff0000"), "COLOR");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#999999");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  Blockly.Blocks.LEDdisply = {
    init: function () {
      this.appendDummyInput()
        .appendField("显示样式: 字体:")
        .appendField(new Blockly.FieldDropdown([["华文黑体", "STHeiti"], ["华文楷体", "STKaiti"], ["华文细黑", "STXihei"], ["华文宋体", "STSong"], ["华文中宋", "STZhongsong"], ["华文仿宋", "STFangsong"], ["华文彩云", "STCaiyun"], ["华文琥珀", "STHupo"], ["华文隶书", "STLiti"], ["华文行楷", "STXingkai"], ["华文新魏", "STXinwei"], ["黑体", "simHei"], ["宋体", "simSun"], ["新宋体", "NSimSun"], ["仿宋", "FangSong"], ["楷体", "KaiTi"], ["仿宋_GB2312", "FangSong_GB2312"], ["楷体_GB2312", "KaiTi_GB2312"], ["隶书", "LiSu"], ["幼圆", "YouYuan"], ["新细明体", "PMingLiU"], ["细明体", "MingLiU"], ["标楷体", "DFKai-SB"], ["微软正黑体", "Microsoft JhengHei"], ["微软雅黑体", "Microsoft YaHei"], ["AcadEref", "AcadEref"], ["Adobe Ming Std L", "Adobe Ming Std L"], ["Adobe Myungjo Std M", "Adobe Myungjo Std M"], ["Adobe Pi Std", "Adobe Pi Std"], ["AIGDT", "AIGDT"], ["AIgerian", "AIgerian"], ["AmdtSymbols", "AmdtSymbols"], ["Arial", "Arial"], ["Arial Rounded MT Bold", "Arial Rounded MT Bold"], ["Arial Unicode MS", "Arial Unicode MS"], ["BankGothic Lt BT", "BankGothic Lt BT"], ["BankGothic Md BT", "BankGothic Md BT"], ["Baskerville Old Face", "Baskerville Old Face"], ["Bauhaus 93", "Bauhaus 93"], ["Beranad MT Condensed", "Beranad MT Condensed"]]), "hz_sharp")
        .appendField(" 字号:")
        .appendField(new Blockly.FieldTextInput("0"), "hz_line_height")
        .appendField("颜色:")
        .appendField(new Blockly.FieldColour("#ff0000"), "color")
        .appendField(" 代码区绘制图案")
        .appendField(new Blockly.FieldCheckbox("true"), "show_hz");
      this.appendDummyInput()
        .appendField("显示位置: 移动:")
        .appendField(new Blockly.FieldDropdown([["上移", "hz_up"], ["下移", "hz_down"]]), "hz_up_down")
        .appendField(new Blockly.FieldTextInput("0"), "hz_up_down_data")
        .appendField("像素 ")
        .appendField(new Blockly.FieldDropdown([["左移", "hz_left"], ["右移", "hz_right"]]), "hz_left_right")
        .appendField(new Blockly.FieldTextInput("0"), "hz_left_right_data")
        .appendField("像素")
        .appendField(" 点阵旋转")
        .appendField(new Blockly.FieldAngle(0), "bitmap_rotate");
      this.appendValueInput("input_data")
        .setCheck(null)
        .appendField("显示内容:");
      this.setInputsInline(false);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#999999");
      this.setTooltip();
      this.setHelpUrl();
    }
  };
  Blockly.Blocks.LED_clear = {
    init: function () {
      this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LED_display.png", 30, 30, "*"))
        .appendField("LED清屏");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#999999");
      this.setTooltip();
      this.setHelpUrl();
    }
  };
  Blockly.Blocks.LED_fill = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LED_display.png", 30, 30, "*"))
        .appendField("LED全屏填充")
        .appendField("颜色:")
        .appendField(new Blockly.FieldColour("#ff0000"), "color_fill");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#999999");
      this.setTooltip();
      this.setHelpUrl();
    }
  };
  Blockly.Blocks.LED_bright = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LED_display.png", 30, 30, "*"))
        .appendField("设置LED屏幕显示亮度")
        .appendField(new Blockly.FieldDropdown([["6", "192"], ["1", "32"], ["2", "64"], ["3", "96"], ["4", "128"], ["5", "160"], ["7", "224"], ["8", "230"]]), "LED_bright");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#999999");
      this.setTooltip("合理设置亮度");
      this.setHelpUrl();
    }
  };
    Blockly.Blocks.WIFI_init = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_WIFI.svg", 30, 30, "*"))
        .appendField("WIFI连接");
      this.appendValueInput("wifi_ssid")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("热点:");
      this.appendValueInput("wifi_password")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("密码:");
      this.setInputsInline(false);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(200);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.WiFi_connect = {
    init: function () {
      this.appendDummyInput()
        .appendField("WiFi连接状态");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour(200);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.WIFI_init_yourSSID = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldMultilineInput("yourSSID"), "yourSSID");
      this.setOutput(true, null);
      this.setColour(200);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.WIFI_init_yourPASSWD = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldMultilineInput("yourPASSWD"), "yourPASSWD");
      this.setOutput(true, null);
      this.setColour(200);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.Aliyun_init = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_Aliyun.svg", 30, 30, "*"))
        .appendField("Aliyun连接");
      this.appendDummyInput()
        .appendField("ProductKey:")
        .appendField(new Blockly.FieldMultilineInput("ProductKey"), "ProductKey");
      this.appendDummyInput()
        .appendField("DeviceName:")
        .appendField(new Blockly.FieldMultilineInput("DeviceName"), "DeviceName");
      this.appendDummyInput()
        .appendField("DeviceSecret:")
        .appendField(new Blockly.FieldMultilineInput("DeviceSecret"), "DeviceSecret");
      this.appendDummyInput()
        .appendField("主题(Topic):");
      this.appendDummyInput()
        .appendField("Topic_0:")
        .appendField(new Blockly.FieldMultilineInput(""), "topic_0");
      this.appendDummyInput()
        .appendField("Topic_1:")
        .appendField(new Blockly.FieldMultilineInput(""), "topic_1");
      this.appendDummyInput()
        .appendField("Topic_2:")
        .appendField(new Blockly.FieldMultilineInput(""), "topic_2");
      this.appendDummyInput()
        .appendField("Topic_3:")
        .appendField(new Blockly.FieldMultilineInput(""), "topic_3");
      this.appendDummyInput()
        .appendField("Topic_4:")
        .appendField(new Blockly.FieldMultilineInput(""), "topic_4");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(99);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.Aliyun_keyvalue = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_Aliyun.svg", 30, 30, "*"))
        .appendField("json键值对:标识符");
      this.appendValueInput("aliyun_key")
        .setCheck(null);
      this.appendDummyInput()
        .appendField("值(数值型)");
      this.appendValueInput("aliyun_value")
        .setCheck(null);
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour(99);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
    Blockly.Blocks.Aliyun_keyvalue_wenb = {
      init: function () {
        this.appendDummyInput()
          .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
          .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_Aliyun.svg", 30, 30, "*"))
          .appendField("json键值对:标识符");
        this.appendValueInput("aliyun_key")
          .setCheck(null);
        this.appendDummyInput()
          .appendField("值(文本型)");
        this.appendValueInput("aliyun_value")
          .setCheck(null);
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(99);
        this.setTooltip("");
        this.setHelpUrl("");
      }
    };
  Blockly.Blocks.Aliyun_ADDkeyvalue = {
    init: function () {
      this.appendValueInput("keyvalue1")
        .setCheck(null);
      this.appendDummyInput()
        .appendField("和");
      this.appendValueInput("keyvalue2")
        .setCheck(null);
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour(99);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.Aliyun_publish = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_Aliyun.svg", 30, 30, "*"))
        .appendField("Aliyun上报");
      this.appendValueInput("publish_data")
        .setCheck(null);
      this.appendDummyInput()
        .appendField("至主题")
        .appendField(new Blockly.FieldDropdown([["Topic_0", "topic_0"], ["Topic_1", "topic_1"], ["Topic_2", "topic_2"], ["Topic_3", "topic_3"], ["Topic_4", "topic_4"]]), "topic1")
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(99);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.Aliyun_keyvalue_text = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_Aliyun.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldMultilineInput("键值对"), "keyvalue_text");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour(99);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  Blockly.Blocks.Aliyun_received_news = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_Aliyun.svg", 30, 30, "*"))
        .appendField("当接收到Aliyun")
        .appendField(new Blockly.FieldDropdown([["Topic_0", "topic_0"], ["Topic_1", "topic_1"], ["Topic_2", "topic_2"], ["Topic_3", "topic_3"], ["Topic_4", "topic_4"]]), "topic3")
        .appendField("的消息");
      this.appendStatementInput("aliyun_callback")
        .setCheck(null);
      this.setColour(99);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.Aliyun_received_json = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_Aliyun.svg", 30, 30, "*"))
        .appendField("获取阿里云返回的信息")
        .appendField("标识符为");
      this.appendValueInput("identifier")
        .setCheck(null);
      this.appendDummyInput()
        .appendField("的参数值");
      this.setOutput(true, null);
      this.setColour(99);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_senddouble= {
    init: function() {
    this.appendValueInput("double")
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .setCheck(null)
        .appendField("(语音播报)离线语音模块串口发送double参数值");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#cc66cc");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_sendint= {
    init: function() {
    this.appendValueInput("int")
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .setCheck(null)
        .appendField("(语音播报)离线语音模块串口发送int参数值");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#cc66cc");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_sendunchar= {
    init: function() {
    this.appendValueInput("unchar")
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .setCheck(null)
        .appendField("(语音播报)离线语音模块串口发送unsginedchar参数值");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#cc66cc");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_sendhead = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(语音播报)")
        .appendField("发送帧头")
        .appendField(new Blockly.FieldMultilineInput("AA55"), "header")
        .appendField("(消息编号)")
        .appendField(new Blockly.FieldMultilineInput("1"), "new_num")
        .appendField("(十六进制字符之间无需空格隔开)");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#cc66cc");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_sendchar= {
    init: function() {
    this.appendValueInput("char")
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .setCheck(null)
        .appendField("(语音播报)离线语音模块串口发送char参数值");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#cc66cc");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_readserial = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(语音识别)循环读取离线语音模块")
        .appendField(new Blockly.FieldDropdown([["Serial", "Serial"], ["Serial1", "Serial1"], ["Serial2", "Serial2"]]), "offline_serial")
        .appendField("数据");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#cc66cc");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_sendTail = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(语音播报)")
        .appendField("发送帧尾")
        .appendField(new Blockly.FieldMultilineInput("55AA"), "tail")
        .appendField("(十六进制字符之间无需空格隔开)");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#cc66cc");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.offline_readserialpanduan = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(语音识别)读取离线语音模块串口数据");
      this.setOutput(true, null);
      this.setColour("#cc66cc");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_rs485_serial_read = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("读取多合一空气质量传感器(RS485)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_rs485_parameter = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(" (需先读取数据) 空气质量中")
        .appendField(new Blockly.FieldDropdown([["温度值(℃)", "LNSFAIoT_air_num_WenDu"], ["湿度值(%)", "LNSFAIoT_air_num_ShiDu"], ["PM10含量(ug/m3)", "LNSFAIoT_air_num_PM10"], ["二氧化碳气体(CO2)含量(ppm)", "LNSFAIoT_air_num_CO2"], ["甲醛气体(CH2O)含量(ug/m3)", "LNSFAIoT_air_num_CH2O"], ["总挥发性有机化合物(TVOC)含量(ug/m3)", "LNSFAIoT_air_num_TVOC"], ["PM2.5含量(ug/m3)", "LNSFAIoT_air_num_PM25"]]), "air_rs485_parameter");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_rs485_serial0println = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("串口0打印显示多合一空气质量传感器(RS485)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_rs485_serialinit = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("初始化多合一空气质量传感器(RS485)设备地址(拨码开关)")
        .appendField(new Blockly.FieldDropdown([["1-3:上下下(0x01)", "1"], ["1-3:下上下(0x02)", "2"], ["1-3:上上下(0x03)", "3"], ["1-3:下下上(0x04)", "4"], ["1-3:上下上(0x05)", "5"], ["1-3:下上上(0x06)", "6"], ["1-3:上上上(0x07)", "7"]]), "air_rs485_addr_choose")
        .appendField("接口")
        .appendField(new Blockly.FieldDropdown([["Serial", "Serial"], ["Serial1", "Serial1"], ["Serial2", "Serial2"]]), "air_rs485_ser");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_uart_serial_read = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("读取多合一空气质量传感器(UART)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_uart_parameter = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(" (需先读取数据) 空气中的")
        .appendField(new Blockly.FieldDropdown([["温度值(℃)", "LNSFAIoT_air_num_WenDu"], ["湿度值(%)", "LNSFAIoT_air_num_ShiDu"], ["PM10含量(ug/m3)", "LNSFAIoT_air_num_PM10"], ["二氧化碳气体(CO2)含量(ppm)", "LNSFAIoT_air_num_CO2"], ["甲醛气体(CH2O)含量(ug/m3)", "LNSFAIoT_air_num_CH2O"], ["总挥发性有机化合物(TVOC)含量(ug/m3)", "LNSFAIoT_air_num_TVOC"], ["PM2.5含量(ug/m3)", "LNSFAIoT_air_num_PM25"]]), "air_uart_parameter");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_uart_serial0println = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("串口0打印显示多合一空气质量传感器(UART)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.air_uart_serialinit = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("初始化多合一空气质量传感器(UART)串口")
        .appendField(new Blockly.FieldDropdown([["Serial", "Serial"], ["Serial1", "Serial1"], ["Serial2", "Serial2"]]), "air_uart_ser");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#3366ff");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.soil_rs485_serial_read = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("读取多合一土质检测传感器(RS485)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#8B4513");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.soil_rs485_parameter = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(" (需先读取数据) 土壤")
        .appendField(new Blockly.FieldDropdown([["PH值", "LNSFAIoT_soil_num_PH"], ["温度值(℃)", "LNSFAIoT_soil_num_WenDu"], ["湿度值(%)", "LNSFAIoT_soil_num_ShiDu"], ["电导率(us/cm)", "LNSFAIoT_soil_num_DianDaoLv"]]), "soil_rs485_parameter");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour("#8B4513");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.soil_rs485_serial0println = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("串口0打印显示多合一土质检测传感器(RS485)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#8B4513");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.soil_rs485_serialinit = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("初始化多合一土质检测传感器(RS485)设备地址(1~254)")
        .appendField(new Blockly.FieldNumber(1, 1, 254, 1), "soil_addr")
        .appendField("串口")
        .appendField(new Blockly.FieldDropdown([["Serial", "Serial"], ["Serial1", "Serial1"], ["Serial2", "Serial2"]]), "soil_rs485_ser");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#8B4513");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.weather_rs485_serial_read = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("读取多合一气象传感器(RS485)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#00008B");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.weather_rs485_parameter = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField(" (需先读取数据) 多合一气象传感器采集的")
        .appendField(new Blockly.FieldDropdown([["风速(m/s)", "LNSFAIoT_num_windspeed"], ["风力(级)", "LNSFAIoT_num_windpower"], ["风向(档位)", "LNSFAIoT_num_winddirection_gear"], ["风向(角度°)", "LNSFAIoT_num_winddirection_angle"], ["湿度值(%RH)", "LNSFAIoT_num_ShiDu"], ["温度值(℃)", "LNSFAIoT_num_WenDu"], ["噪声分贝(dB)", "LNSFAIoT_num_noise"], ["大气压力(Kpa)", "LNSFAIoT_num_airpressure"], ["光照强度(Lux)", "LNSFAIoT_num_light"]]), "weather_rs485_parameter");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour("#00008B");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.weather_rs485_serial0println = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("串口0打印显示多合一气象传感器(RS485)采集的数据");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#00008B");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.weather_rs485_serialinit = {
    init: function () {
      this.appendDummyInput()
        .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"));
      this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("初始化多合一气象传感器(RS485)设备地址(1~254)")
        .appendField(new Blockly.FieldNumber(1, 1, 254, 1), "weather_addr")
        .appendField("串口")
        .appendField(new Blockly.FieldDropdown([["Serial", "Serial"], ["Serial1", "Serial1"], ["Serial2", "Serial2"]]), "weather_rs485_ser");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#00008B");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.MP3PLAYMODE = {
    init: function () {
      this.appendDummyInput()
        .appendField("设置单线串口MP3的播放模式为")
        .appendField(new Blockly.FieldDropdown([["播放", "1"], ["暂停", "2"], ["停止", "3"], ["上一曲", "4"], ["下一曲", "5"]]), "playmode");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#d28535");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.MP3Volume = {
    init: function () {
      this.appendDummyInput()
        .appendField("设置单线串口MP3的播放音量为(0~100)")
        .appendField(new Blockly.FieldNumber(20, 0, 100, 1), "Volume");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#d28535");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.MP3SONGNUM = {
    init: function () {
      this.appendDummyInput()
        .appendField("设置单线串口MP3的播放第")
        .appendField(new Blockly.FieldNumber(1, 0, 9999, 1), "songnum")
        .appendField("首歌曲 (歌曲必须放在根目录下，且曲目号的命名格式必须是五位数字组成");
      this.appendDummyInput()
        .appendField("如 00001.mp3表示该歌的曲目号为1，以此类推)");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#d28535");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  Blockly.Blocks.MP3PIN = {
    init: function () {
      this.appendValueInput("PIN")
        .setCheck(null)
        .appendField("初始化单线串口MP3通信引脚为");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#d28535");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };



  Blockly.Blocks.InfraredrayinitA= {
    init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("初始化红外学习发射模块串口为")
        .appendField(new Blockly.FieldDropdown([["Serial1","Serial1"],["Serial2","Serial2"]]), "SERIALQA");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#F48FB1");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };

Blockly.Blocks.StudyA= {
  init: function() {
  this.appendDummyInput()
  .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
      .appendField("(红外学习发射模块)学习红外编号")
      .appendField(new Blockly.FieldTextInput("01"), "StudyA")
      .appendField("(红外编号的范围为01-F8)");
  this.setPreviousStatement(true, null);
  this.setNextStatement(true, null);
  this.setColour("#F48FB1");
  this.setTooltip("");
  this.setHelpUrl("");
  }
};
Blockly.Blocks.SendA= {
  init: function() {
  this.appendDummyInput()
  .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
      .appendField("(红外学习发射模块)发射红外编号")
      .appendField(new Blockly.FieldTextInput("01"), "SendA")
      .appendField("(红外编号的范围为01-F8)");
  this.setPreviousStatement(true, null);
  this.setNextStatement(true, null);
  this.setColour("#F48FB1");
  this.setTooltip("");
  this.setHelpUrl("");
  }
};
Blockly.Blocks.printaA= {
  init: function() {
  this.appendDummyInput()
  .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
      .appendField("串口0打印红外编号")
      .appendField(new Blockly.FieldTextInput("01"), "printaA")
      .appendField("(红外编号的范围为01-F8)");
  this.setPreviousStatement(true, null);
  this.setNextStatement(true, null);
  this.setColour("#F48FB1");
  this.setTooltip("");
  this.setHelpUrl("");
  }
};
Blockly.Blocks.printqA= {
  init: function() {
  this.appendDummyInput()
  .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
      .appendField("串口0打印(红外学习发射模块)直接读取到的红外码");
  this.setPreviousStatement(true, null);
  this.setNextStatement(true, null);
  this.setColour("#F48FB1");
  this.setTooltip("");
  this.setHelpUrl("");
  }
};
Blockly.Blocks.clearA= {
  init: function() {
  this.appendDummyInput()
  .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
      .appendField("(红外学习发射模块)清除学习数据");
  this.setPreviousStatement(true, null);
  this.setNextStatement(true, null);
  this.setColour("#F48FB1");
  this.setTooltip("");
  this.setHelpUrl("");
  }
};
  Blockly.Blocks.OfflineSpeechread= {
    init: function () {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(初始化离线语音模块串口为")
        .appendField(new Blockly.FieldDropdown([["Serial1","Serial1"],["Serial2","Serial2"]]), "SERIALA")
        .appendField("数据");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour("#00cccc");
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };
  
    Blockly.Blocks.OfflineSpeechdata= {
      init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
          .appendField("(语音识别)读取到离线语音模块的串口数据");
      this.setOutput(true, null);
      this.setColour("#00cccc");
      this.setTooltip("");
      this.setHelpUrl("");
      }
    };
  
  Blockly.Blocks.Speechrecognition= {
      init: function() {
      this.appendDummyInput()
      .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
          .appendField("(语音识别)离线语音模块识别到")
          .appendField(new Blockly.FieldDropdown([["学习空调","F0"],["学习电视","F1"],["学习风扇","F2"],["学习灯具","F3"],["学习小车","F4"],["学习自定义设备一","F5"],["学习自定义设备二","F6"],["学习自定义设备三","F7"],["学习自定义设备四","F8"]]), "Identify")
          .appendField("(学习红外码)");
      this.setOutput(true, null);
      this.setColour("#00cccc");
      this.setTooltip("");
      this.setHelpUrl("");
      }
    };
    
  Blockly.Blocks.VoiceRecognition= {
    init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(语音识别)离线语音模块识别到")
        .appendField(new Blockly.FieldDropdown([["打开空调","A0"],["关闭空调","A1"],["打开风速","A2"],["关闭风速","A3"],["模式切换","A4"],["升高温度","A5"],["降低温度","A6"],["上下扫风","A7"],["左右扫风","A8"],["停止上下扫风","A9"],["停止左右扫风","AA"],["打开健康|节能","AB"],["关闭健康|节能","AC"],["打开干燥辅热","AD"],["关闭干燥辅热","AE"],["打开温度显示","AF"],["关闭温度显示","BD"],["打开睡眠","BE"],["关闭睡眠","BF"],["打开超强","FA"],["关闭超强","FB"],["打开定时","CE"],["关闭定时","CF"],["打开电视","B0"],["关闭电视","B1"],["打开机顶盒","B2"],["关闭机顶盒","B3"],["信号源切换","B4"],["打开菜单","B5"],["菜单确认","B6"],["菜单返回","B7"],["上一频道","B8"],["下一频道","B9"],["增加音量","BA"],["减小音量","BB"],["电视静音","BC"],["打开风扇","C0"],["关闭风扇","C1"],["风扇调速","C2"],["风扇定时","C3"],["打开摇头","C4"],["关闭摇头","C5"],["一挡风","C6"],["二挡风","C7"],["三档风","C8"],["四挡风","C9"],["五档风","CA"],["六档风","CB"],["打开灯光","D0"],["关闭灯光","D1"],["灯光调亮","D2"],["灯光调暗","D3"],["灯光变色","D4"],["前进","E0"],["后退","E1"],["停止","E2"],["左转","E3"],["右转","E4"],["加速","E5"],["减速","E6"],["巡线模式","E7"],["跟随模式","E8"],["避障模式","E9"],["巡线避障模式","EA"],["设备一操作一","D5"],["设备一操作二","D6"],["设备一操作三","D7"],["设备一操作四","D8"],["设备一操作五","D9"],["设备二操作一","DA"],["设备二操作二","DB"],["设备二操作三","DC"],["设备二操作四","DD"],["设备二操作五","DE"],["设备三操作一","DF"],["设备三操作二","EB"],["设备三操作三","EC"],["设备三操作四","ED"],["设备三操作五","EE"],["设备四操作一","EF"],["设备四操作二","FC"],["设备四操作三","FD"],["设备四操作四","FE"],["设备四操作五","FF"]]), "Recognition")
        .appendField("(学习红外码)");
    this.setOutput(true, null);
    this.setColour("#00cccc");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };
  
  Blockly.Blocks.Broadcast= {
    init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(语音播报)离线语音模块播报")
        .appendField(new Blockly.FieldDropdown([["请按关闭空调键","54"],["请按打开风速键","55"],["请按关闭风速键","56"],["请按模式切换","57"],["请按升高温度键","58"],["请按降低温度键","59"],["请按上下扫风键","60"],["请按左右扫风键","61"],["请按停止上下扫风键","62"],["请按停止左右扫风键","63"],["请按打开健康|节能键","64"],["请按关闭健康|节能键","65"],["请按打开干燥辅热键","66"],["请按关闭干燥辅热键","67"],["请按打开温度显示键","68"],["请按关闭温度显示键","69"],["请按打开睡眠键","70"],["请按关闭睡眠键","71"],["请按打开超强键","72"],["请按关闭超强键","73"],["请按打开定时键","76"],["请按关闭定时键","77"],["请按关闭电视键","1"],["请按打开机顶盒键","2"],["请按关闭机顶盒键","3"],["请按信号源切换键","4"],["请按打开菜单键","5"],["请按菜单确认键","6"],["请按菜单返回键","7"],["请按上一频道键","8"],["请按下一频道键","9"],["请按增加音量键","10"],["请按减小音量键","11"],["请按电视静音键","12"],["请按打开风扇键","13"],["请按风扇调速键","14"],["请按风扇定时键","15"],["请按打开摇头键","16"],["请按关闭摇头键","17"],["请按一挡风键","18"],["请按二挡风键","19"],["请按三档风键","30"],["请按四挡风键","21"],["请按五档风键","22"],["请按六档风键","23"],["请按关闭灯光键","24"],["请按灯光调亮键","30"],["请按灯光调暗键","26"],["请按灯光变色键","27"],["请按后退键","28"],["请按停止键","29"],["请按左转键","30"],["请按右转键","31"],["请按加速键","32"],["请按减速键","33"],["请按巡线模式键","34"],["请按跟随模式键","35"],["请按避障模式键","36"],["请按巡线避障模式键","37"],["设备一操作二","38"],["设备一操作三","39"],["设备一操作四","40"],["设备一操作五","41"],["设备二操作二","42"],["设备二操作三","43"],["设备二操作四","44"],["设备二操作五","45"],["设备三操作二","46"],["设备三操作三","47"],["设备三操作四","48"],["设备三操作五","49"],["设备四操作二","50"],["设备四操作三","51"],["设备四操作四","52"],["设备四操作五","53"]]), "Broadcast")
        .appendField("(学习红外码)");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#00cccc");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };
  
  Blockly.Blocks.Podcast= {
    init: function() {
    this.appendDummyInput()
    .appendField(new Blockly.FieldImage("../../media/LNSF_AIoT_LOGO.svg", 30, 30, "*"))
        .appendField("(语音播报)离线语音模块播报")
        .appendField(new Blockly.FieldDropdown([["已打开空调","78"],["已关闭空调","79"],["已打开风速","80"],["已关闭风速","81"],["模式已切换","82"],["温度已升高","83"],["温度已降低","84"],["已打开上下扫风","85"],["已打开左右扫风","86"],["已停止上下扫风","87"],["已停止左右扫风","88"],["已打开健康|节能","89"],["已关闭健康|节能","90"],["已打开干燥辅热","91"],["已关闭干燥辅热","92"],["已打开温度显示","93"],["已关闭温度显示","94"],["已打开睡眠","95"],["已关闭睡眠","96"],["已打开超强","97"],["已关闭超强","98"],["已打开定时","99"],["已关闭定时","100"],["已打开电视","101"],["已关闭电视","102"],["已打开机顶盒","103"],["已关闭机顶盒","104"],["信号源已切换","105"],["已打开菜单","106"],["菜单已确认","107"],["菜单已返回","108"],["已切换至上一频道","109"],["已切换至下一频道","110"],["已增加音量"," 111"],["已减小音量","112"],["电视已静音","113"],["已打开风扇","114"],["已关闭风扇","115"],["风扇已调速","116"],["风扇已定时","117"],["已打开摇头","118"],["已关闭摇头","119"],["已打开一挡风","130"],["已打开二挡风","121"],["已打开三档风","122"],["已打开四挡风","123"],["已打开五档风","124"],["已打开六档风","130"],["灯光已打开","126"],["灯光已关闭","127"],["灯光已调亮","128"],["灯光已调暗","129"],["灯光已变色","130"],["小车已前进","131"],["小车已后退","132"],["小车已停止","133"],["小车已左转","134"],["小车已右转","135"],["小车已加速","136"],["小车已减速","137"],["小车已进入巡线模式","138"],["小车已进入跟随模式","139"],["小车已进入避障模式","140"],["小车已进入巡线避障模式","141"],["已执行设备一操作一","142"],["已执行设备一操作二","143"],["已执行设备一操作三","144"],["已执行设备一操作四","145"],["已执行设备一操作五","146"],["已执行设备二操作一","147"],["已执行设备二操作二","148"],["已执行设备二操作三","149"],["已执行设备二操作四","150"],["已执行设备二操作五","151"],["已执行设备三操作一","152"],["已执行设备三操作二","153"],["已执行设备三操作三","154"],["已执行设备三操作四","155"],["已执行设备三操作五","156"],["已执行设备四操作一","157"],["已执行设备四操作二","158"],["已执行设备四操作三","159"],["已执行设备四操作四","160"],["已执行设备四操作五","161"]]), "Podcast")
        .appendField("(语音控制)");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour("#00cccc");
    this.setTooltip("");
    this.setHelpUrl("");
    }
  };

})();